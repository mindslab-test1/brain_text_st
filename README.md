# Text Style Transfer

## 소개

백트랜슬래이션 기반의 단순한 방법으로 텍스트의 스타일을 변경하는 엔진입니다.

## 서빙

### 도커 이미지

```brain_text_st:{version}스타일-server``` 형식으로 작성되어 있는 도커 이미지들 중에서 원하는 스타일의 이미지를 고르시면 됩니다. ```brain_text_st:{version}-diverse``` 이미지도 필요하니 같이 받으시기 바랍니다.

### docker run 이용하기

#### run_text_style_transfer_service.py

문장과 스타일에 대한 정보를 받아 해당하는 스타일 변환 모델에 전해주는 역할을 합니다. 각 스타일에 대한 모델을 가지고 있는 서비스의 아이피와 포트 정보를  ```src/serving/text_style_transfer_gateway_config.yaml``` 파일에 적습니다. 사용할 포트와 아이피에 더불어 이전에 작성한 설정 파일의 경로도 함께 넘겨 줍니다. 특정 모델이 사용되는 것은 아니기 때문에 ```brain_text_st:1.0.0-diverse``` 이미지가 아닌 ```brain_text_st:1.0.0``` 등의 다른 이미지가 사용되어도 무방합니다.

```
docker run \
	--rm \
	-p 18290 \
	-v $(pwd)/src/serving/text_style_transfer_gateway_config.yaml:/root/tst/serving/text_style_transfer_gateway_config.yaml \
	docker.maum.ai:443/brain/brain_text_st:1.0.0-diverse \
	python run_text_style_transfer_service.py \
    	--ip [::] \
      	--port 18290 \
      	--config_file_path serving/text_style_transfer_gateway_config.yaml
```

#### run_inverse_paraphraser.py

실제 스타일 변환이 일어나는 부분입니다. 각 스타일마다 도커 이미지가 하나씩 존재합니다. 아래의 예제 코드에서는 성경 스타일에 해당하는 이미지가 사용된 것을 볼 수 있습니다. 원하는 아이피와 포트 넘버를 지정하고 CPU를 사용할 것인지 GPU를 사용할 것인지 설정합니다. 아래는 CPU를 이용하는 설정이고 GPU 이용 설정은 아래 나올 ```run_diverse_paraphraser.py``` 예제에서 보실 수 있는 것과 같이 하시면 됩니다. 각 inverse paraphraser는 다음에 등장하는 하나의 공통된 diverse paraphraser의 아이피와 포트 넘버 정보를 추가로 가집니다. 물론 diverse paraphraser가 꼭 하나여야 하는 것은 아닙니다.

아래와 같이 적절한 포트 넘버를 고르는 작업을 반복하여 성경, 해시태그 등 원하는 스타일에 대한 모델들을 준비 시킵니다. 현재 사내 도커 레지스트리에 있는 스타일은 성경, 해시태그, 트위터, 메신저, 스피치, 논픽션, 개인 대화, 대본을 포함하여 총 8개 입니다. 존재하는 모델들이 서로 비슷한 문장들을 생성하는 경우도 많기 때문에 8개가 다 필요하다고 느껴지지 않으실 수 있습니다. 그 경우 ```serving/text_style_transfer_gateway_config.yaml```에서 필요하지 않은 부분들을 지우셔도 좋을 것 같습니다.

```
docker run \
	-p 18292 \
	docker.maum.ai:443/brain/brain_text_st:1.0.0-bible-server \
	python run_inverse_paraphraser_service.py \
    	--ip [::] \
      	--port 18292 \
      	--device cpu \
      	--diverse_paraphraser_ip 114.108.173.120 \
      	--diverse_paraphraser_port 18291
```

#### run_diverse_paraphraser.py

4번 GPU를 사용하도록 설정된 예입니다. 위에서처럼 CPU를 사용하는 것도 가능합니다. 각 스타일마다 하나씩 존재하는 inverse paraphraser와 달리 diverse paraphraser는 하나만 존재해도 무방합니다. 존재 개수와는 상관 없이 하나의 inverse paraphraser는 꼭 하나의 diverse paraphraser를 필요로 합니다.

```
docker run \
	-p 18291 \
	--gpus device=4 \
	docker.maum.ai:443/brain/brain_text_st:1.0.0-diverse \
	python run_diverse_paraphraser_service.py \
    	--ip [::] \
      	--port 18291 \
      	--device cuda:0
```

### docker-compose 이용하기

위의 내용은 함께 들어 있는 ```docker-compose.yml``` 파일을 이용하여 실행될 수도 있습니다. ```docker-compose.yml``` 파일에 게이트웨이라고 적혀 있는 컨터테이너의 ```src/serving/text_style_transfer_gateway_config.yaml``` 설정 파일에 각 스타일 변환을 담당하는 컨테이너와의 소통에 필요한 아이피 주소와 포트를 작성합니다. 스타일 변환을 담당하는 ```bible, hashtag, twitter``` 등의 이름을 가진 컨테이너는 ```.env``` 파일에 적힌 ```diverse_paraphraser```의 아이피 주소와 포트 번호를 통해 소통합니다.

아래는 diverse paraphraser의 아이피와 포트 넘버를 가지고 있는  ```.env``` 파일의 예입니다.

```DIVERSE_PARAPHRASER_IP=114.108.173.120 
DIVERSE_PARAPHRASER_IP=114.108.173.120 
DIVERSE_PARAPHRASER_PORT=18291 
```

```docker-compose version 1.25.4, build 8d51620a```에서 작동을 확인하였습니다.

### 클라이언트 확인

정상적으로 작동하는지 확인하기 위해 아래의 명령을 실행합니다.

```python -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. serving/text_style_transfer.proto```

```src/run_client.py```에 ```run_text_style_transfer_service.py```에 입력한 아이피와 포트 넘버를 넘겨서 정상적으로 작동하는지 확인합니다.

```
python run_client.py --ip 114.108.173.120 --port 18290
input: 설정 파일들이 너무 여러 군데 흩어져 있는 것 같습니다
Number of sentences to generate: 3
Style: bible
설정 파일이 여러 곳에 흩어진 것 같구나
나의 설정 파일이 여러 곳에 흩어진 것 같구나
설정 파일이 여러 곳에 흩어졌나이다
```

```src/run_client.py```에 ```--iterate_config ``` 설정을 주고 ```config_file_path```의 아규먼트로 ```text_style_transfer_gateway_config.yaml``` 파일의 경로를 넘기면 설정 파일이 가지고 있는 전체 스타일에 대한 결과를 볼 수 있습니다.

```
input: ICLR 2021 너무 기대돼요!
Number of sentences to generate: 1
bible
ICL 2021은 참으로 기대되도다

hashtag
#ICL 2021이 너무 기대된다!

twitter
ICL 2021 너무 기대되네...!!

messenger
ICL 2021 너무 기대되요!!

speech
정말 기대되는데요.

non_fiction
ICL 2021은 너무도 기대된다!

personal_conversation
아이고 아이고 아이고 아이고 아이고 아이고 아이고 아이고 아이고

script
ICL 2021이 너무 기대된다!
```

### GPU or CPU?

GPU 자원이 한정적이라면 ```run_diverse_paraphraser.py```에 우선적으로 사용하시고 그 다음으로는 비교적 더 마음에 드는 스타일에 대한 모델에 GPU를 할당하시면 좋을 것 같습니다. 저는 개인적으로 성경, 해시태그, 트위터, 메신저, 논픽션, 개인 대화, 스피치, 대본 순으로 선호합니다. ```run_text_style_transfer_service.py```에는 GPU가 할당되지 않아도 됩니다.

## 학습

학습은 크게 데이터 생성, 데이터 필터링, 모델 학습 세 구간으로 나뉩니다. 지금은 학습이 번거롭게 되어 있어서 추후 타겟 스타일에 대한 데이터를 넣으면 모델을 바로 학습하는 식으로 코드가 수정되면 좋을 것 같습니다. GPU는 4번 GPU가 쓰인다고 가정하였습니다.

### 주의사항

빠르게 실험해 보기 위해 작성했던 부분들을 고치지 않아서 모델 학습을 직접하기 번거로운 면이 있습니다. 혹시 학습이 필요하게 되는 경우를 대비하여 문서는 작성하였지만 학습이 편하게 진행될 수 있도록 코드가 수정되면 매뉴얼도 바뀔 것이기 때문에 아래의 예제를 따라서 직접 학습이 진행된 적은 없었습니다. 코드는 실제 학습 코드와 같은 코드이지만 이런 점 때문에 학습 절차 진행 중 예기치 않은 에러가 생성될 수도 있습니다.

### 데이터 생성

문잔 단위로 개행된 텍스트 파일인 my_data.txt를 학습시키고 싶은 상황을 가정하겠습니다. 아래는 학습 파일 예입니다.

```
커피를 먹으면 잠이 잘 오지 않는 것 같아요.
저 어제 잠을 하나도 안 잔 것에 더불어 지금 새벽 한 시가 넘었는데 졸리지 않아요.
이 문서 사실 금요일까지 작성해야 하는 건데 지금 토요일입니다.
```

해당 파일과 생성된 데이터가 저장될 디렉터리를 아래와 같이 볼륨으로 지정합니다. 빠른 생성을 위해 GPU를 사용하시기를 권장드리고 특별한 경우가 아니라면 다른 아규먼트들은 바꾸지 않으시기를 권해 드립니다.

```
docker run \
	--gpus device=4 \
	-v $(pwd)/path/to/my_data.txt:/root/tst/data/my_data.txt \
	-v $(pwd)/path/to/dest_dir:/root/tst/dest_dir \
    docker.maum.ai:443/brain/brain_text_st:1.0.0-diverse \
    python3 generate_multi_diverse_paraphrase_data.py \
        --src_file_path             data/my_data.txt \
        --paraphrase_dest_dir       dest_dir \
        \
        --tokenizer_type            sentencepiece \
        --tokenizer_path            kogpt2/kogpt2_unk_chars_added.spiece \
        --model_type                gpt2 \
        --seed                      0 \
        --top_k                     0 \
        --top_p                     0.4 \
        --model_max_length          200 \
        --temperature               1.0 \
        --num_beams                 1 \
        --model_checkpoint_path     checkpoints/model.ckpt \
        --device                    cuda:0 \
        --num_samples_per_p         5 \
        --max_num_tok_per_line      80 \
        --batch_tok_limit           1000 \
        --max_src_to_tgt_ratio      2.0 \
        --do_sample
```

아래는 설정되어야 할 부분을 복사한 것입니다.

```
--gpus device=4 \
-v $(pwd)/path/to/my_data.txt:/root/tst/data/my_data.txt \
-v $(pwd)/path/to/dest_dir:/root/tst/dest_dir \
--src_file_path             data/my_data.txt \
--paraphrase_dest_dir       dest_dir \
```

### 데이터 필터링

위의 단계가 끝나면 my_data_original.txt와 함께 my_data_normalized_p=0.4_s=*.txt 형식의 파일이 여러 개 생겼을 겁니다. 그 파일의 경로들을 아래와 같이 입력하여 데이터를 필터링 하시면 됩니다. 이 데이터를 가지고 테스트를 진행하지는 않기 때문에 테스트셋 비율은 선택적인 아규먼트입니다. 원하시면 ```--test_ratio 0.1```  등으로 추가해 주셔도 됩니다. 모델 평가와 관련된 부분은 현재의 도커 이미지에 추가 되어 있지 않습니다.

```
docker run \
	--gpus device=4 \
	-v $(pwd)/path/to/dest_dir:/root/tst/dest_dir \
    docker.maum.ai:443/brain/brain_text_st:1.0.0-diverse \
	python3 split_multi_dataset.py \
        --src_file_path \
            dest_dir/my_data_normalized_p=0.4_s=0.txt \
			dest_dir/my_data_normalized_p=0.4_s=1.txt \
            dest_dir/my_data_normalized_p=0.4_s=2.txt \
            dest_dir/my_data_normalized_p=0.4_s=3.txt \
            dest_dir/my_data_normalized_p=0.4_s=4.txt \
        --tgt_file_path             dest_dir/my_data_original.txt \
        --valid_ratio               0.1 \
        --repetition_min_length     15 \
        --repetition_threshold      0.15 \
        --tokenizer_type            sentencepiece \
        --tokenizer_path            kogpt2/kogpt2_unk_chars_added.spiece
```

아래는 설정되어야 할 부분을 복사한 것입니다.

```
--gpus device=4 \
-v $(pwd)/path/to/dest_dir:/root/tst/dest_dir \
--src_file_path \
dest_dir/my_data_normalized_p=0.4_s=0.txt \
dest_dir/my_data_normalized_p=0.4_s=1.txt \
dest_dir/my_data_normalized_p=0.4_s=2.txt \
dest_dir/my_data_normalized_p=0.4_s=3.txt \
dest_dir/my_data_normalized_p=0.4_s=4.txt \
--tgt_file_path             dest_dir/my_data_original.txt \
--valid_ratio               0.1 \
# optional
--test_ratio                0.1 \
```

### 모델 학습

데이터 필터링을 통해 생성된 파일들을 아래와 같이 넣어 모델을 학습합니다.

```
python3 train_paraphraser.py \
    --checkpoint_file_name                  my_model \
    --save_top_k                            1 \
    --diverse_paraphraser_checkpoint_path   checkpoints/model.ckpt \
    --early_stopping_patience               3 \
    --model_type                            gpt2 \
    --pretrained_path                       kogpt2/kogpt2_pretrained \
    --tokenizer_type                        sentencepiece \
    --tokenizer_path                        kogpt2/kogpt2_unk_chars_added.spiece \
\
    --learning_rate                         5.0e-05 \
    --lr_decay_patience                     0 \
    --lr_decay_factor                       0.5 \
\
    --train_src_file_path_list  \
                                            dest_dir/my_data_normalized_p=0.4_s=0_train.txt \
                                            dest_dir/my_data_normalized_p=0.4_s=1_train.txt \
                                            dest_dir/my_data_normalized_p=0.4_s=2_train.txt \
                                            dest_dir/my_data_normalized_p=0.4_s=3_train.txt \
                                            dest_dir/my_data_normalized_p=0.4_s=4_train.txt \
    --val_src_file_path_list   \
                                            dest_dir/my_data_normalized_p=0.4_s=0_val.txt \
                                            dest_dir/my_data_normalized_p=0.4_s=1_val.txt \
                                            dest_dir/my_data_normalized_p=0.4_s=2_val.txt \
                                            dest_dir/my_data_normalized_p=0.4_s=3_val.txt \
                                            dest_dir/my_data_normalized_p=0.4_s=4_val.txt \
    --train_tgt_file_path                   dest_dir/my_data_original_train.txt \
    --val_tgt_file_path                     dest_dir/my_data_original_val.txt \

    --first_token_id                        0 \
    --last_token_id                         1 \
    --intermediate_token_id                 2 \
    --pad_token_id                          3 \
    --num_workers                           10 \
    --batch_size                            20 \
    --dataset_style                         map \
    --check_max_token_length                \
\
    --gpus                                  1 \
    --log_every_n_steps                     10 \
    --progress_bar_refresh_rate             10 \
    --check_val_every_n_epoch               1 \
    --max_epochs                            10000 \
    --accumulate_grad_batches               1 \
    --default_root_dir                      dest_dir/checkpoints
```

아래는 필수적으로 지정되어야 하는 부분들입니다.

```
--checkpoint_file_name                  my_model \
--train_src_file_path_list  \
    dest_dir/my_data_normalized_p=0.4_s=0_train.txt \
    dest_dir/my_data_normalized_p=0.4_s=1_train.txt \
    dest_dir/my_data_normalized_p=0.4_s=2_train.txt \
    dest_dir/my_data_normalized_p=0.4_s=3_train.txt \
    dest_dir/my_data_normalized_p=0.4_s=4_train.txt \
--val_src_file_path_list   \
    dest_dir/my_data_normalized_p=0.4_s=0_val.txt \
    dest_dir/my_data_normalized_p=0.4_s=1_val.txt \
    dest_dir/my_data_normalized_p=0.4_s=2_val.txt \
    dest_dir/my_data_normalized_p=0.4_s=3_val.txt \
    dest_dir/my_data_normalized_p=0.4_s=4_val.txt \
--default_root_dir                      dest_dir/checkpoints
--train_tgt_file_path                   dest_dir/my_data_original_train.txt \
--val_tgt_file_path                     dest_dir/my_data_original_val.txt \
```

아래는 선택적으로 변경하셔도 되는 부분들입니다.

```
--learning_rate                         5.0e-05 \
--lr_decay_patience                     0 \
--lr_decay_factor                       0.5 \
```

### 학습 확인

학습된 모델은 ```dest_dir/checkpoints``` 디렉터리 아래에 저장됩니다. 텐서보드로 로깅이 진행되기 때문에 상위 디렉터리 등을 감시하도록 텐서보드를 실행하시면 학습 결과도 함께 보실 수 있습니다.