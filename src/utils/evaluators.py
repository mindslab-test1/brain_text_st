import torch
from transformers import (
    BertForPreTraining,
    BertTokenizer,
)
from data_processing.tokenizers import (
    HuggingFaceBertTokenizerWrapper,
)
from nltk.translate.bleu_score import sentence_bleu
from typing import List, Any
import pathlib, json
from utils.semantic_similarity import (
    bertscore, calculate_bertscore_from_hidden_states, 
    pooler_cosine_similarity, 
    last_hidden_cls_cosine_similarity, 
    last_hidden_mean_cosine_similarity,
)
import statistics
from torch.nn.utils.rnn import pad_sequence
import os, yaml
import numpy as np

class SemanticEvaluator:    
    def __init__(self):
        pass

    def on_test_epoch_start(_, self, model_attribute) -> None:
        # self.bert_for_evaluation = BertForPreTraining(bert_config)
        if self.hparams.evaluation_model_type == 'BertForPreTraining':
            self.bert_for_evaluation = BertForPreTraining.from_pretrained(self.hparams.evaluation_model_path)
            self.bert_for_evaluation = self.bert_for_evaluation.bert

        if self.hparams.evaluation_tokenizer_type == 'bert_tokenizer':
            bert_tokenizer = BertTokenizer.from_pretrained(self.hparams.evaluation_tokenizer_path)
            self.tokenizer = HuggingFaceBertTokenizerWrapper(bert_tokenizer)
        print('device: ', self.device)
        self.eval()
        self.to(self.device)

    @torch.no_grad()
    def test_step(_, self, src_tgt_pair, model_attribute, batch_idx, dataloader_idx=None,):
        # Generate a test sample
        src_lines, tgt_line = src_tgt_pair
        src_ids_list = [self.batch_processor.encode(src_line) for src_line in src_lines]
        generation_batch = self.batch_processor.make_batch_for_generation([src_ids_list])
        generation_batch.to(self.device)

        generated_tensor = self.generate_wrapper(
            batch=generation_batch,
            do_sample=self.hparams.do_sample,
            top_p=self.hparams.top_p,
            top_k=self.hparams.top_k,
            max_length=self.hparams.max_length, temperature=self.hparams.temperature,
            num_beams=self.hparams.num_beams,
            num_return_sequences=self.hparams.num_return_sequences,
        )
        generated_line = self.batch_processor.decode(generated_tensor.squeeze().tolist())

        input_ids = [
            torch.tensor([self.hparams.evaluation_cls_token_id] + self.tokenizer.encode(tgt_line)),
            torch.tensor([self.hparams.evaluation_cls_token_id] + self.tokenizer.encode(generated_line))]
        input_ids = pad_sequence(input_ids, padding_value=self.hparams.evaluation_pad_token_id, batch_first=True)
        attention_mask = torch.logical_not(input_ids == self.hparams.evaluation_pad_token_id).long()
        input_ids = input_ids.to(self.device)
        attention_mask = attention_mask.to(self.device)

        bert_output = self.bert_for_evaluation(input_ids, attention_mask=attention_mask, return_dict=True)
        last_hidden_state = bert_output['last_hidden_state']
        pooler_output = bert_output['pooler_output']
        # stylized_hidden_state, tgt_hidden_state = torch.chunk(last_hidden_state, 2)
        # stylized_attention_mask, tgt_attention_mask = torch.chunk(attention_mask, 2)
        bert_score = calculate_bertscore_from_hidden_states(last_hidden_state[:, 1:, :], attention_mask[:, 1:])[0]
        last_cls_score = last_hidden_cls_cosine_similarity(last_hidden_state)[0]
        pooler_score = pooler_cosine_similarity(pooler_output)[0]
        last_hidden_mean_score = last_hidden_mean_cosine_similarity(last_hidden_state, attention_mask)[0]

        bleu_score = sentence_bleu(
            references=[tgt_line.split()],
            hypothesis=generated_line.split(),
            weights=(1.0,),
        ) 

        print(f'tgt_line:\t\t {tgt_line}')
        print(f'diverse_lines:\t\t {src_lines}')
        print(f'generated_line:\t\t {generated_line}')
        print(f'bleu_score:\t\t {bleu_score}')
        print(f'bert_score:\t\t {bert_score}')
        print(f'last_hidden_mean_score:\t {last_hidden_mean_score}')
        print(f'last_cls_score:\t\t {last_cls_score}')
        print(f'pooler_score:\t\t {pooler_score}')
        print(' ')
        
        return {
            'original': tgt_line,
            'stylized': generated_line,
            'bleu_score': bleu_score,
            'bert_score': 0 if np.isnan(bert_score) else bert_score,
            'last_cls_score': last_cls_score,
            'pooler_score': pooler_score,
            'last_hidden_mean_score': last_hidden_mean_score,
        }

    def test_epoch_end(_, self, outputs: List[Any], model_attribute) -> None:
        destination_dir = pathlib.Path(self.hparams.default_root_dir) / 'evaluation'
        os.makedirs(destination_dir, exist_ok=True)
        with open(destination_dir / 'evaluation_output.txt', 'w') as f:
            for output in outputs:
                for key in output:
                    value = output[key] 
                    if isinstance(output[key], str):
                        value = value.strip() 
                    print(f'{key}:\t\t {value}', file=f)
                print('', file=f)

        with open(destination_dir / 'evaluation_output.json', 'w') as f:
            json.dump(outputs, f)

        with open(destination_dir / 'evaluation_results.txt', 'w') as f:
            print(f'bleu_score mean:\t {statistics.mean([output["bleu_score"] for output in outputs])}', file=f)
            print(f'bert_score mean:\t {statistics.mean([output["bert_score"] for output in outputs])}', file=f)
            print(f'last_hidden_mean_score mean:\t {statistics.mean([output["last_hidden_mean_score"] for output in outputs])}', file=f)
            print(f'last_cls_score mean:\t {statistics.mean([output["last_cls_score"] for output in outputs])}', file=f)
            print(f'pooler_score mean:\t {statistics.mean([output["pooler_score"] for output in outputs])}', file=f)

        with open(destination_dir / 'hparams.yaml', 'w') as f:
            yaml.dump(self.hparams, f, default_flow_style=False)

        print(self.hparams)
        print(f'bleu_score mean:\t {statistics.mean([output["bleu_score"] for output in outputs])}')
        print(f'bert_score mean:\t {statistics.mean([output["bert_score"] for output in outputs])}')
        print(f'last_hidden_mean_score mean:\t {statistics.mean([output["last_hidden_mean_score"] for output in outputs])}')
        print(f'last_cls_score mean:\t {statistics.mean([output["last_cls_score"] for output in outputs])}')
        print(f'pooler_score mean:\t {statistics.mean([output["pooler_score"] for output in outputs])}')
        print('num_beams', self.hparams.num_beams)
        print('top_p', self.hparams.top_p)