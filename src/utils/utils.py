def pad_left(lines, padding_value):
    """
    insert pad tokens to the left of the input sequences.
    Args:
        lines ([type]): [description]
        padding_value ([type]): [description]

    Returns:
        [type]: [description]
    """
    num_rows = len(lines)
    num_cols = max(line.size(0) for line in lines)
    output = lines[0].data.new(num_rows, num_cols).fill_(padding_value)
    for i, line in enumerate(lines):
        length = line.size(0)
        output[i, num_cols - length:] = line
    return output

def get_num_lines(file_path):
    num_lines = 0
    with open(file_path, 'r') as f:
        for line in f:
            num_lines += 1
    return num_lines