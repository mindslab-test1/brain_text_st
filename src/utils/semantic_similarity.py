import torch

def bertscore(hyp_hidden_states, ref_hidden_states, hyp_attention_masks, ref_attention_masks):
    """
    Reimplementation of official BERTScore.
    idf, all_layer options are removed, and the chances of it containing a bug has increased by a lot.
    B denotes the batch size, S denotes the sequence length, h denotes the hidden state size.
    Don't forget to remove cls, sep embeddings since the score is only meant for word embeddings.

    Args:
        hyp_hidden_states (torch.Tensor): float tensor of size B, S, h.
        ref_hidden_states (torch.Tensor: float tensor of size B, S, h.
        hyp_attention_masks (torch.Tensor): long tensor of size B, S
        ref_attention_masks (torch.Tensor): long tensor of size B, S

    Returns:
        Dict[str, torch.Tensor]: dict of torch tensors whose sizes are all B.
    """
    # normalize by Frobenius norm so that the resulting similarity metrix has cosine similarity for each of its values
    # div(B S h, B, S 1) -> B S h
    hyp_hidden_states.div_(torch.norm(hyp_hidden_states, dim=-1).unsqueeze(-1))
    ref_hidden_states.div_(torch.norm(ref_hidden_states, dim=-1).unsqueeze(-1))

    # bmm(B S h, B h S) -> B S S. 
    # Each of its values is cosine a similarity score.
    similarity_matrix = torch.bmm(hyp_hidden_states, ref_hidden_states.transpose(2, 1))

    # bmm(B S 1, B 1 S) -> B S S. 
    # 1 means both of the entries are not masked, and 0 means at least one of them is masked.
    masking_matrix = torch.bmm(hyp_attention_masks.float().unsqueeze(-1), ref_attention_masks.float().unsqueeze(1))
    
    # set the masked entries' similarity scores to -inf.
    similarity_matrix[masking_matrix == 0] = float('-inf')

    # max(B S1 S2) -> B S1. (keep in mind that max returns a tuple.)
    precision = similarity_matrix.max(dim=2)[0]
    # max(B S1 S2) -> B S2
    recall = similarity_matrix.max(dim=1)[0]

    # isinf(B S1) -> B S1
    # turn inf values to 0 to calcuating the mean values.
    precision_is_inf = torch.isinf(precision)
    precision[precision_is_inf] = 0

    # sum(B S1) -> B. sum(B S1) -> B. div(B, B) -> B
    # calculate the mean of the max values.
    precision = precision.sum(dim=1) / (~precision_is_inf).float().sum(dim=1)

    # isinf(B S2) -> B S2
    # turn inf values to 0 to calcuating the mean values.
    recall_is_inf = torch.isinf(recall)
    recall[recall_is_inf] = 0

    # sum(B S2) -> B. sum(B S2) -> B. div(B, B) -> B
    # calculate the mean of the max values.
    recall = recall.sum(dim=1) / (~recall_is_inf).float().sum(dim=1)

    # elementwise arithmetic.
    f1_score = 2 * precision * recall / (precision + recall)

    return {
        'precision': precision,
        'recall': recall,
        'f1_score': f1_score,
    }

def calculate_bertscore_from_hidden_states(hidden_states, pad_msk):
    stylized_hidden_state, tgt_hidden_state = torch.chunk(hidden_states, 2)
    stylized_attention_mask, tgt_attention_mask = torch.chunk(pad_msk, 2)
    bertscores = bertscore(
        hyp_hidden_states=stylized_hidden_state,
        ref_hidden_states=tgt_hidden_state,
        hyp_attention_masks=stylized_attention_mask,
        ref_attention_masks=tgt_attention_mask,
    )['f1_score'].tolist()
    return bertscores
    
def last_hidden_cls_cosine_similarity(hidden_states):
    embedding = hidden_states[:, 0, :]
    cls_states_1, cls_states_2 = torch.chunk(embedding, 2)
    return torch.cosine_similarity(cls_states_1, cls_states_2, dim=1).tolist()

def pooler_cosine_similarity(pooler_output):
    embedding = pooler_output
    cls_states_1, cls_states_2 = torch.chunk(embedding, 2)
    return torch.cosine_similarity(cls_states_1, cls_states_2, dim=1).tolist()

def last_hidden_mean_cosine_similarity(hidden_states, pad_msk):
    mask_filtered_hidden_states = pad_msk.unsqueeze(-1) * hidden_states
    summed_hidden_states = torch.sum(mask_filtered_hidden_states, 1)
    num_tokens = (pad_msk == 1).sum(dim=1)
    embedding = torch.true_divide(summed_hidden_states, num_tokens.view(-1, 1))
    cls_states_1, cls_states_2 = torch.chunk(embedding, 2)
    return torch.cosine_similarity(cls_states_1, cls_states_2, dim=1).tolist()