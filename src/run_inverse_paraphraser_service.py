from transformers import BertTokenizer
from serving import decorators, utils
import logging
import grpc
from concurrent import futures
from argparse import ArgumentParser
import torch
import sentencepiece as spm
from serving.text_style_transfer_pb2_grpc import (
    InverseParaphraserServicer, 
    add_InverseParaphraserServicer_to_server,
    DiverseParaphraserStub,
)
from serving.text_style_transfer_pb2 import (
    InverseParaphraserRequest, 
    InverseParaphraserResponse,
    DiverseParaphraserRequest,
)
from encoder_decoder_paraphraser.encoder_decoder_paraphraser import (
    LitBert2BertParaphraser, 
)
from decoder_only_paraphraser.decoder_only_paraphraser import (
    LitGPT2Paraphraser,
)
from data_processing.tokenizers import (
    HuggingFaceBertTokenizerWrapper,
    SentencePieceWrapper,
)
# from legacy.paraphraser import LitGPT2Paraphraser as LitKoGPT2Paraphraser

class InverseParaphraserServicer(InverseParaphraserServicer): 
    def __init__(self, args):
        if args.model_type == 'bert2bert':
            self.inverse_paraphraser = LitBert2BertParaphraser.load_from_checkpoint(
                checkpoint_path=args.checkpoint_path,
                map_location=torch.device(args.device),
            )
            self.num_diverse_lines_required = len(self.inverse_paraphraser.hparams.train_src_file_path_list)
        elif args.model_type == 'gpt2':
            self.inverse_paraphraser = LitGPT2Paraphraser.load_from_checkpoint(
                checkpoint_path=args.checkpoint_path,
                map_location=torch.device(args.device),
            )
            self.num_diverse_lines_required = len(self.inverse_paraphraser.hparams.train_src_file_path_list)
        # elif args.model_type == 'kogpt2':
        #     # TODO: Do not load the model this way.
        #     self.inverse_paraphraser = LitKoGPT2Paraphraser.load_from_checkpoint(
        #         args.checkpoint_path,
        #         map_location=args.device,
        #     )
        #     self.inverse_paraphraser.hparams['first_token_id'] = 0
        #     self.inverse_paraphraser.hparams['last_token_id'] = 1
        #     self.inverse_paraphraser.hparams['pad_token_id'] = 3
        #     self.inverse_paraphraser.hparams['intermediate_token_id'] = 2
        #     # TODO: CHANGE THIS
        #     self.num_diverse_lines_required = 5
        #     # self.inverse_paraphraser = LitGPT2Paraphraser(model.hparams)
        #     # self.inverse_paraphraser.decoder_only = model.gpt2
        else:
            raise ValueError('Invalid model_type argument.')

        # TODO: I do not want to write tokenizer type checking code everytime it is needed,
        # please write a method or function that does this for me.
        if args.tokenizer_type == 'bert_tokenizer':
            bert_tokenizer = BertTokenizer.from_pretrained(args.tokenizer_path)
            tokenizer = HuggingFaceBertTokenizerWrapper(bert_tokenizer)
        elif args.tokenizer_type == 'sentencepiece':
            sp_model = spm.SentencePieceProcessor(args.tokenizer_path)
            tokenizer = SentencePieceWrapper(sp_model)
        else:
            raise ValueError('Invalid tokenizer_type argument.')
        self.inverse_paraphraser.load_batch_processor(tokenizer)

        self.args = args
        self.inverse_paraphraser.to(args.device)
        self.inverse_paraphraser.eval()
        print(f'Model is running on {self.inverse_paraphraser.device}')

    # TODO: Consiter using interceptors.
    @decorators.log_exceptions(logging)
    @decorators.log_remote_calls(logging)
    @decorators.validate_input_length_with('max_input_length')
    @decorators.validate_num_return_sequences_with('max_num_return_sequences')
    def GenerateInverseParaphrase(self, request: InverseParaphraserRequest, context):
        # TODO: Consider using async.
        with grpc.insecure_channel(f'{args.diverse_paraphraser_ip}:{args.diverse_paraphraser_port}') as channel:
            try:
                diverse_paraphraser_request = DiverseParaphraserRequest(
                    original_text=request.original_text,
                    num_return_sequences=self.num_diverse_lines_required
                )
                diverse_paraphraser_stub = DiverseParaphraserStub(channel)
                diverse_paraphraser_response = diverse_paraphraser_stub.GenerateDiverseParaphrase(diverse_paraphraser_request)
            except grpc.RpcError as rpc_error:
                if rpc_error.code() == grpc.StatusCode.ABORTED:
                    logging.error(f'Inverse paraphraser has aborted while processing the request. \nrequest: ({request})')
                    context.abort(
                        code=grpc.StatusCode.ABORTED,
                        details='The input sentence may have been too long or too complex, please try calling with a simpler sentence.',
                    )
                else:
                    raise
        # This check is not really necessary, but it's better to be safe than sorry. 
        if len(diverse_paraphraser_response.paraphrased_texts) != self.num_diverse_lines_required:
            logging.error('# of sentences the diverse paraphraser has returned does not match the configured number.')
            context.abort(
                code=grpc.StatusCode.ABORTED,
                details='The input sentence may have been too long or too complex, please try calling with a simpler sentence.',)
        
        # Check if the input token length does not exceed the model's max token length.
        ids_list = [self.inverse_paraphraser.batch_processor.encode(paraphrased_text) for paraphrased_text in diverse_paraphraser_response.paraphrased_texts]
        inverse_paraphraser_batch = self.inverse_paraphraser.batch_processor.make_batch_for_generation([ids_list])
        inverse_paraphraser_batch = inverse_paraphraser_batch.to(args.device)

        if inverse_paraphraser_batch.get_input_ids().size(-1) > self.args.max_input_token_ids_length_from_diverse:
            logging.error('The # of tokens that the diverse paraphraser has generated exceeds the configured maximum value of the inverse paraphraser.')
            context.abort(
                code=grpc.StatusCode.ABORTED,
                details='The input sentence may have been too long or too complex, please try calling with a simpler sentence.',)

        with torch.no_grad():
            stylized_texts = self.inverse_paraphraser.generate_wrapper(
                batch=inverse_paraphraser_batch,
                top_p=self.args.top_p,
                num_return_sequences=request.num_return_sequences,
                temperature=self.args.temperature,
                num_beams=self.args.num_beams,
                top_k=self.args.top_k,
                do_sample=self.args.do_sample,
                max_length=self.args.max_length,
                return_str=True,
                device=self.args.device,
            )
        if len(stylized_texts) != request.num_return_sequences:
            logging.error(f'There was at least one incomplete sentence in the returned sentences from the inverse paraphraser\'s generate_wrapper call. \nrequest: ({request})')
            context.abort(
                code=grpc.StatusCode.ABORTED,
                details='The input sentence may have been too long or too complex, please try calling with a simpler sentence.',
            )
        return InverseParaphraserResponse(paraphrased_texts=stylized_texts)

def serve(args):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=args.num_workers))
    add_InverseParaphraserServicer_to_server(
        InverseParaphraserServicer(args),
        server,
    )
    server.add_insecure_port(f'{args.ip}:{args.port}')
    server.start()
    print('The service has been started.')
    server.wait_for_termination()

def add_argparse_args():
    parser = ArgumentParser()
    parser.add_argument('--ip', type=str, default='[::]')
    parser.add_argument('--port', type=int, default=50051)
    parser.add_argument('--num_workers', type=int, default=1)
    parser.add_argument('--logging_level', type=str, default='INFO')
    parser.add_argument('--log_file_path', type=str, default=f'log/run_inverse_paraphraser_service.log')

    parser.add_argument('--diverse_paraphraser_ip', type=str, default='127.0.0.1')
    parser.add_argument('--diverse_paraphraser_port', type=int, default=50051)

    parser.add_argument('--model_type', type=str, default='gpt2')
    parser.add_argument('--checkpoint_path', type=str, default='checkpoints/model.ckpt')
    parser.add_argument('--tokenizer_type', type=str, default='sentencepiece')
    parser.add_argument('--tokenizer_path', type=str, default='kogpt2/kogpt2_unk_chars_added.spiece')
    parser.add_argument('--device', type=str, default='cuda:0')
    
    parser.add_argument('--max_input_length', type=int, default=80)
    parser.add_argument('--max_num_return_sequences', type=int, default=5)
    parser.add_argument('--max_input_token_ids_length_from_diverse', type=int, default=400)
    # Decoding related args
    parser.add_argument('--top_p', type=float, default=1.0)
    parser.add_argument('--top_k', type=int, default=0)
    parser.add_argument('--temperature', type=float, default=1.0)
    parser.add_argument('--num_beams', type=int, default=5)
    parser.add_argument('--max_length', type=int, default=80)
    parser.add_argument('--do_sample', action='store_true')

    return parser

if __name__ == '__main__':
    parser = add_argparse_args()
    args = parser.parse_args()
    utils.set_logger(args.log_file_path, args.logging_level)
    serve(args)
