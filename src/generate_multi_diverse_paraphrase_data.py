from os import makedirs
from transformers.models.bert.tokenization_bert import BertTokenizer
# from legacy.paraphraser import LitGPT2Paraphraser as LitKoGPT2Paraphraser
from argparse import ArgumentParser 
import torch
import yaml
import pprint
import sys, logging
import sentencepiece as spm
from contextlib import ExitStack
import pathlib
from tqdm import tqdm
import pytorch_lightning as pl
from transformers import set_seed
import collections
import matplotlib.pyplot as plt
import utils.utils as utils
from encoder_decoder_paraphraser.encoder_decoder_paraphraser import (
    LitEncoderDecoderParaphraser,
    LitBert2BertParaphraser,
)
from decoder_only_paraphraser.decoder_only_paraphraser import (
    LitDecoderOnlyParaphraser,
    LitGPT2Paraphraser,
)
from data_processing.batch_processors import (
    EncoderDecoderBatchProcessor,
    DecoderOnlyBatchProcessor,
)
from data_processing.tokenizers import (
    HuggingFaceBertTokenizerWrapper, 
    SentencePieceWrapper
)

class StreamDataset(torch.utils.data.IterableDataset):
    def __init__(self, path, batch_processor, batch_tok_limit, max_num_tok_per_line, do_not_filter, max_num_lines=None):
        super().__init__()
        self.path = path
        self.batch_processor = batch_processor
        self.batch_tok_limit = batch_tok_limit
        self.max_num_tok_per_line = max_num_tok_per_line
        self.num_lines_yielded = 0
        self.num_exceeded = 0
        self.do_not_filter = do_not_filter
        self.max_num_lines = max_num_lines
        if self.max_num_lines is None:
            self.total_num_lines = utils.get_num_lines(self.path)
        else:
            self.total_num_lines = self.max_num_lines

    def __iter__(self):
        cur_max_tok_len = 0
        input_ids_list = []
        raw_strings = []

        pbar = tqdm(desc=pathlib.Path(self.path).name, total=self.total_num_lines)
        num_lines_read = 0
        with open(self.path, 'r') as lines:
            for line_str in lines:
                pbar.update(1)
                pbar.set_postfix({'num_exceeded': self.num_exceeded})
                line_tok = self.batch_processor.encode(line_str)

                if len(line_tok) > self.max_num_tok_per_line:
                    self.num_exceeded += 1
                    logging.info(
                        f'Exceeded maximum # of tokens per line {len(line_tok)} > {self.max_num_tok_per_line} || # exceeded: {self.num_exceeded} || {line_str.strip()}')
                    continue

                self.num_lines_yielded += 1
                if len(line_tok) > cur_max_tok_len:
                    cur_max_tok_len = len(line_tok)

                if self.batch_tok_limit <= (len(input_ids_list) + 1) * cur_max_tok_len:
                    batch = self.batch_processor.make_batch_for_generation([[input_ids] for input_ids in input_ids_list])

                    yield raw_strings, batch
                    raw_strings = [line_str]
                    input_ids_list = [line_tok]
                    cur_max_tok_len = 0
                else:
                    raw_strings.append(line_str)
                    input_ids_list.append(line_tok)

                num_lines_read += 1
                if self.max_num_lines is not None and num_lines_read >= self.max_num_lines:
                    break

            if len(input_ids_list) > 0:
                batch = self.batch_processor.make_batch_for_generation([[input_ids] for input_ids in input_ids_list])
                yield raw_strings, batch
            logging.info(f'Exceeded: {self.num_exceeded} || Total # of lines {self.num_lines_yielded}')
            pbar.close()
    
    def __len__(self):
        # TODO: Remove this method.
        return self.total_num_lines
    
    def num_lines_read(self):
        # TODO: Remove this method.
        return self.num_exceeded + self.num_lines_yielded

# TODO: The below lines are really messy... Please do clean them up...
if __name__ == '__main__':
    parser = ArgumentParser()
    # parser.add_argument('--first_token_id', type=int, required=True)
    # parser.add_argument('--last_token_id', type=int, required=True)
    # parser.add_argument('--intermediate_token_id', type=int, required=True)
    # parser.add_argument('--pad_token_id', type=int, required=True)
    parser.add_argument('--src_file_path', required=True)
    parser.add_argument('--model_type', type=str, required=True)
    parser.add_argument('--tokenizer_type', type=str, required=True)
    parser.add_argument('--tokenizer_path', type=str, required=True)
    parser.add_argument('--paraphrase_dest_dir', required=True)
    parser.add_argument('--seed', type=int)
    parser.add_argument('--top_k', type=float, required=True)
    parser.add_argument('--temperature', type=float, default=1.0)
    parser.add_argument('--model_max_length', type=int, required=True)
    parser.add_argument('--top_p', type=float, nargs='+', required=True)
    parser.add_argument('--num_beams', type=int, required=True)
    parser.add_argument('--model_checkpoint_path', type=str, required=True)
    parser.add_argument('--device', type=str, required=True)
    parser.add_argument('--num_samples_per_p', type=int, required=True)
    parser.add_argument('--max_num_tok_per_line', type=int, required=True)
    parser.add_argument('--batch_tok_limit', type=int, required=True)
    parser.add_argument('--do_sample', action='store_true')
    parser.add_argument('--max_src_to_tgt_ratio', type=float, required=True)
    parser.add_argument('--do_not_filter', action='store_true')
    parser.add_argument('--max_num_lines', type=int)
    args = parser.parse_args()

    path_obj = pathlib.Path(args.src_file_path)
    dir_name = path_obj.parent
    file_prefix = path_obj.stem
    dest_prefix = dir_name / file_prefix
    paraphrase_dest_path=pathlib.Path(args.paraphrase_dest_dir) / (file_prefix + '_normalized.txt')
    original_dest_path=pathlib.Path(args.paraphrase_dest_dir) / (file_prefix + '_original.txt')

    makedirs(args.paraphrase_dest_dir, exist_ok=True)

    pathlib.Path(args.paraphrase_dest_dir).mkdir(parents=True, exist_ok=True)
    logging.basicConfig(
        filename=f'{args.paraphrase_dest_dir}/generate_paraphrases_{file_prefix}.log',
        filemode='w',
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        level=logging.DEBUG,
    )

    if args.seed is not None:
        pl.utilities.seed.seed_everything(args.seed)
        set_seed(args.seed)
    
    pprint.pprint(vars(args))
    with open(pathlib.Path(args.paraphrase_dest_dir) / 'paraphrase_generation_config.yaml', 'w') as config_f:
        yaml.dump(vars(args), config_f, default_flow_style=False)
    logging.info(args)

    logging.info(f'Starting... {args.src_file_path} | {paraphrase_dest_path} | {original_dest_path} ')

    if args.model_type == 'gpt2':
        model = LitGPT2Paraphraser.load_from_checkpoint(
            args.model_checkpoint_path,
            map_location=args.device,
        )
    # elif args.model_type == 'kogpt2':
    #     model = LitKoGPT2Paraphraser.load_from_checkpoint(
    #         args.model_checkpoint_path,
    #         map_location=args.device,
    #     )
    #     model.hparams['first_token_id'] = 0
    #     model.hparams['last_token_id'] = 1
    #     model.hparams['pad_token_id'] = 3
    #     model.hparams['intermediate_token_id'] = 2
    elif args.model_type == 'bert2bert':
        model = LitBert2BertParaphraser.load_from_checkpoint(
            args.model_checkpoint_path,
            map_location=args.device,
        )
    model.eval()
    model.to(args.device)
    
    if args.tokenizer_type == 'bert_tokenizer':
        tokenizer = HuggingFaceBertTokenizerWrapper(BertTokenizer.from_pretrained(args.tokenizer_path))
    elif args.tokenizer_type == 'sentencepiece':
        sp_model = spm.SentencePieceProcessor(args.tokenizer_path)
        tokenizer = SentencePieceWrapper(sp_model)

    print(model.hparams)
    batch_processor = model.load_batch_processor(tokenizer)

    stream_dataset = StreamDataset(
        path=args.src_file_path, 
        batch_processor=batch_processor,
        batch_tok_limit=args.batch_tok_limit, 
        max_num_tok_per_line=args.max_num_tok_per_line,
        do_not_filter=args.do_not_filter,
        max_num_lines=args.max_num_lines,
    )

    dataloader = torch.utils.data.DataLoader(
        dataset=stream_dataset,
        batch_size = None,
        shuffle=False,
        drop_last=False,
    )        

    with \
        ExitStack() as stack, \
        open(original_dest_path, 'w') as orig_file:

        paraphrase_dest_path_obj = pathlib.Path(paraphrase_dest_path)
        para_files = [
            stack.enter_context(
                open(
                    paraphrase_dest_path_obj.parent / \
                    (
                        paraphrase_dest_path_obj.stem + \
                        '_p=' + \
                        str(p) + \
                        '_s=' + \
                        str(file_idx) + \
                        paraphrase_dest_path_obj.suffix
                    ),
                    'w')
                ) for p in args.top_p for file_idx in range(args.num_samples_per_p)
        ]
        for para_file in para_files:
            print(para_file.name)

        num_dropped = 0
        num_max_identical_samples = []
        for original_lines, batch in dataloader:
            outputs = []
            for p in args.top_p:
                output = model.generate_wrapper(
                    batch=batch.to(args.device),
                    top_p=p,
                    num_return_sequences=args.num_samples_per_p,
                    temperature=args.temperature,
                    num_beams=args.num_beams,
                    top_k=args.top_k, 
                    do_sample=args.do_sample,
                    # TODO: take care of this mess
                    max_length=min(
                        args.model_max_length, 
                        int(batch.get_input_ids().size(-1) * args.max_src_to_tgt_ratio)
                    ),
                )
                outputs.append(output)

            outputs_list = [output.tolist() for output in outputs]
            for batch_idx in range(batch.get_input_ids().size(0)):
                should_be_skipped = False
                # len_exceeded = False
                paraphrases = []
                original = original_lines[batch_idx]
                for i, output in enumerate(outputs_list):
                    for sample_idx in range(args.num_samples_per_p):
                        generated = output[batch_idx * args.num_samples_per_p + sample_idx]
                        try:
                            eos_idx = generated.index(batch_processor.last_token_id)
                        except ValueError:
                            eos_idx = len(generated)
                        decoded = batch_processor.decode(generated[:eos_idx])  
                        encoded = batch_processor.encode(decoded)
                        if batch_processor.last_token_id not in generated:
                            logging.info(
                                f"""Generation has been interrupted. 
                                len(generated): {len(generated)} 
                                # dropped: {num_dropped} 
                                decoded: {decoded} 
                                original: {original.strip()} 
                                p: {args.top_p[i]}
                                """
                            )
                            if not args.do_not_filter:
                                should_be_skipped = True
                        if len(encoded) > args.max_num_tok_per_line:
                            logging.info(
                                f"""Exceeded maximum # of tokens per line.
                                len(generated): {len(generated)} 
                                # dropped: {num_dropped} 
                                decoded: {decoded} 
                                original: {original.strip()}
                                p: {args.top_p[i]}
                                """
                            )
                            if not args.do_not_filter:
                                should_be_skipped = True
                        paraphrases.append(decoded)

                if not should_be_skipped:
                    counter = collections.Counter(paraphrases)    
                    _, num_identical = counter.most_common(1)[0]  
                    num_max_identical_samples.append(num_identical)
                    print(original, file=orig_file, end='')
                    for paraphrase, para_file in zip(paraphrases, para_files):
                        print(paraphrase, file=para_file)
                else:
                    num_dropped += 1

        logging.info(
            f'# dropped: {num_dropped}'
        )
        max_identical_samples_counter = collections.Counter(num_max_identical_samples)
        logging.info(
            f"""max_identical_samples_counter: {max_identical_samples_counter}"""
        )
        
        max_identical_samples_cnt = []
        for i in range(1, args.num_samples_per_p + 1):
            if i in max_identical_samples_counter:
                max_identical_samples_cnt.append(max_identical_samples_counter[i])
            else:
                max_identical_samples_cnt.append(0)
        plt.bar(range(1, args.num_samples_per_p + 1), max_identical_samples_cnt)
        plt.savefig(pathlib.Path(args.paraphrase_dest_dir) / 'max_identical_count.png')
