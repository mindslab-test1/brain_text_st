import pytorch_lightning as pl 
from argparse import ArgumentParser
import torch
from transformers import (
    EncoderDecoderModel,
)
from data_processing.paraphrase_batch import (
    EncoderDecoderParaphraseBatch
)
from data_processing.batch_processors import (
    EncoderDecoderBatchProcessor,
)
from typing import List, Any
import pprint
from utils.evaluators import SemanticEvaluator

class LitEncoderDecoderParaphraser(pl.LightningModule):
    
    def __init__(self, hparams):
        super().__init__()
        self.save_hyperparameters(hparams)
        if self.hparams.pretrained_path is not None:
            self.encoder_decoder = EncoderDecoderModel.from_pretrained(self.hparams.pretrained_path)
    
    @staticmethod
    def add_argparse_args(parent_parser):
        parser = ArgumentParser(parents=[parent_parser], add_help=False)
        parser.add_argument('--learning_rate', type=float, default=5.0e-05)
        parser.add_argument('--use_segment_embedding', action='store_true') 
        parser.add_argument('--lr_decay_patience', type=int) 
        parser.add_argument('--lr_decay_factor', type=float) 
        return parser
    
    def forward(self, *args, **kwargs):
        pass
    
    def training_step(self, batch, batch_idx):
        # encoder_input_ids = batch.encoder_input_ids # decoder_intput_ids = batch.decoder_input_ids # lebels = batch.labels 
        output = self.encoder_decoder(
            input_ids=batch.encoder_input_ids,
            attention_mask=batch.encoder_attention_mask,
            decoder_input_ids=batch.decoder_input_ids,
            labels=batch.labels,
            return_dict=True,
        )
        loss = output['loss'] 
        self.log('train_loss', loss, on_step=True, on_epoch=True, prog_bar=True, logger=True)
        self.log('train_perp', torch.exp(loss), on_step=True, on_epoch=True, prog_bar=True, logger=True)
        return loss

    # def training_step_end(self):
    #     pass

    # def training_epoch_end(self):
    #     pass

    def validation_step(self, batch, batch_idx):
        output = self.encoder_decoder(
            input_ids=batch.encoder_input_ids,
            attention_mask=batch.encoder_attention_mask,
            decoder_input_ids=batch.decoder_input_ids,
            labels=batch.labels,
            return_dict=True,
        )
        loss = output['loss'] 
        self.log('val_loss', loss, prog_bar=True, on_epoch=True, logger=True)
        self.log('val_perp', torch.exp(loss), prog_bar=True, on_epoch=True, logger=True)
        return loss

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(
            self.parameters(), 
            lr=self.hparams.learning_rate,
            betas=(0.9, 0.999),
        )
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            optimizer=optimizer,
            mode='min',
            factor=self.hparams.lr_decay_factor,
            patience=self.hparams.lr_decay_patience,
            threshold=0.0,
            threshold_mode='abs',
        )
        return {
            'optimizer': optimizer,
            'lr_scheduler': scheduler,
            'monitor': 'val_loss',
        }
    
    def on_test_epoch_start(self) -> None:
        self.evaluator = SemanticEvaluator()
        return self.evaluator.on_test_epoch_start(self, 'encoder_decoder')

    def test_step(self, src_tgt_pair, batch_idx, dataloader_idx=None):
        return self.evaluator.test_step(
            self=self, 
            src_tgt_pair=src_tgt_pair, 
            model_attribute='encoder_decoder', 
            batch_idx=batch_idx, 
            dataloader_idx=dataloader_idx,
        )

    def test_epoch_end(self, outputs: List[Any]) -> None:
        return self.evaluator.test_epoch_end(
            self=self, 
            outputs=outputs,
            model_attribute='encoder_decoder'
        )
    
    @property
    def batch_processor(self):
        return self._batch_processor
    
    @batch_processor.setter
    def batch_processor(self, batch_processor):
        self._batch_processor = batch_processor

    @batch_processor.deleter
    def batch_processor(self):
        del self._batch_processor

    def generate_inverse_paraphraser_input_ids(
        self,
        top_p, 
        device,
        input_ids,
        diverse_num_return_sequences,
        temperature,
        num_beams,
        top_k,
        do_sample,
        max_length,
    ):
    #TODO: Add batch generation.
        input_ids = torch.cat(
            (
                torch.tensor(self.hparams.first_token_id, dtype=torch.long, device=device).view(1, -1),
                torch.tensor(input_ids, dtype=torch.long, device=device).view(1, -1),
                torch.tensor(self.hparams.last_token_id, dtype=torch.long, device=device). view(1, -1),
            ),
            dim=-1,
        )
        # input_ids = batch.input_ids
        # normalized_idx = torch.zeros([1, 0], dtype=torch.long, device=device)
        # print(input_ids)
        output_ids_list = [torch.tensor(self.hparams.first_token_id, device=device).view(1, -1)]
        for i, (p, num_return_sequence) in enumerate(zip(top_p, diverse_num_return_sequences)):
            generated_tensor = self.encoder_decoder.generate(
                input_ids=input_ids, 
                do_sample=do_sample,
                top_p=p,
                top_k=top_k,
                max_length=max_length, 
                temperature=temperature,
                pad_token_id=self.hparams.pad_token_id,
                eos_token_id=self.hparams.last_token_id,
                num_beams=num_beams,
                num_return_sequences=num_return_sequence,
                decoder_start_token_id=self.hparams.first_token_id
            )
            # TODO: is for loop the best I can do?
            for j, generated in enumerate(torch.split(generated_tensor, 1)):
                # Slice the eos token
                # TODO: Make it loop only once
                if self.hparams.last_token_id in generated:
                    # print(generated)
                    eos_idx = (generated == self.hparams.last_token_id).nonzero(as_tuple=False)[0]
                    generated = generated[:, 1:eos_idx[-1]]
                    # print(generated)
                # Slice the first pad token 
                # TODO: If there is no eos token in the generated ids, then there can't be a pad token, too.
                # elif self.hparams.pad_token_id in generated:
                #     pad_idx = (generated == self.hparams.pad_token_id).nonzero(as_tuple=False)[0]
                #     generated = generated[:, 1:pad_idx[-1]]
                else:
                    generated = generated[:, 1:]
                # If it is the last sentence, append intermediate_token
                if j == generated_tensor.size(0) - 1 and i == len(top_p) - 1:
                    output_ids_list.append(generated)
                    output_ids_list.append(torch.tensor(self.hparams.intermediate_token_id, device=device).view(1, -1))
                # If it is not the last sentence, append last_token
                else:
                    output_ids_list.append(generated)
                    output_ids_list.append(torch.tensor(self.hparams.last_token_id, device=device).view(1, -1))
        output_ids = torch.cat(output_ids_list, dim=-1)
        # print(output_ids)
        # print(self.hparams.first_token_id)
        # TODO: Batch processing is not yet supported, so there is no need to make attention masks.
        # encoder_attention_mask = (output_ids != self.hparams.pad_token_id).long()
        batch = EncoderDecoderParaphraseBatch(
            encoder_input_ids=output_ids, 
            encoder_attention_mask=None
        )
        return batch
    
    def generate_wrapper(
        self, 
        top_p, 
        num_return_sequences,
        temperature,
        num_beams,
        top_k,
        do_sample,
        max_length,
        return_str=False,
        batch=None,
        texts='',
        device=None,
    ):
        # print(self.hparams.first_token_id, self.hparams.intermediate_token_id, self.hparams.last_token_id)
        # print(batch.encoder_input_ids)
        # print(batch.encoder_attention_mask)
        if batch is None:
            if type(texts) is str:
                texts = [texts]
            encoded_texts = [self.batch_processor.encode(text) for text in texts]
            batch = self.batch_processor.make_batch_for_generation([encoded_texts])
        if device is not None:
            batch = batch.to(device)
        
        # Just use the max length the caller has provided, 
        # even if there is a chance that the value might exceed the model's max length.
        # max_length = min(self.encoder_decoder.config.decoder.max_position_embeddings, max_length)
        # try:
        #     max_length = min(self.batch_processor.decoder_max_token_length, max_length)
        # except:
        #     max_length = max_length
        generated_tensor = self.encoder_decoder.generate(
            input_ids=batch.encoder_input_ids, 
            attention_mask=batch.encoder_attention_mask,
            do_sample=do_sample,
            top_p=top_p,
            top_k=top_k,
            max_length=max_length, 
            temperature=temperature,
            pad_token_id=self.hparams.pad_token_id,
            eos_token_id=self.hparams.last_token_id,
            num_beams=num_beams,
            num_return_sequences=num_return_sequences,
            decoder_start_token_id=self.hparams.first_token_id
        )
        if return_str:
            return [self.batch_processor.decode(ids) for ids in generated_tensor.tolist() if self.hparams.last_token_id in ids]
        else:
            return generated_tensor
    
class LitBert2BertParaphraser(LitEncoderDecoderParaphraser):
    
    def __init__(self, hparams):
        super().__init__(hparams)
    
    def load_batch_processor(self, tokenizer,):
        batch_processor = EncoderDecoderBatchProcessor(
            first_token_id=self.hparams.first_token_id,
            last_token_id=self.hparams.last_token_id,
            intermediate_token_id=self.hparams.intermediate_token_id,
            pad_token_id=self.hparams.pad_token_id,
            tokenizer=tokenizer,
            encoder_max_token_length=self.encoder_decoder.config.encoder.max_position_embeddings,
            decoder_max_token_length=self.encoder_decoder.config.decoder.max_position_embeddings,
        )
        self._batch_processor = batch_processor
        return batch_processor
        