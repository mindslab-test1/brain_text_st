from serving import decorators, utils
import logging, grpc, yaml
from concurrent import futures
from argparse import ArgumentParser
from serving.text_style_transfer_pb2_grpc import (
    TextStyleTransferServicer, 
    add_TextStyleTransferServicer_to_server,
    InverseParaphraserStub,
)
from serving.text_style_transfer_pb2 import (
    TextStyleTransferRequest, 
    TextStyleTransferResponse,
    InverseParaphraserRequest,
)
# from legacy.paraphraser import LitGPT2Paraphraser as LitKoGPT2Paraphraser

class TextStyleTransferServicer(TextStyleTransferServicer): 
    def __init__(self, args):
        with open(args.config_file_path, 'r') as f:
            self.config = yaml.safe_load(f)
        self.args = args
        
    # TODO: Consiter using interceptors.
    @decorators.log_exceptions(logging)
    @decorators.log_remote_calls(logging)
    @decorators.validate_text_style_transfer_requeset
    def GenerateStylizedText(self, request: TextStyleTransferRequest, context):
        # TODO: Consider using async.
        service_config = self.config[request.target_style]
        with grpc.insecure_channel(f"{service_config['ip']}:{service_config['port']}") as channel:
            try:
                inverse_paraphraser_request = InverseParaphraserRequest(
                    original_text=request.original_text,
                    num_return_sequences=request.num_return_sequences,
                )
                inverse_paraphraser_stub = InverseParaphraserStub(channel)
                inverse_paraphraser_response = inverse_paraphraser_stub.GenerateInverseParaphrase(inverse_paraphraser_request)
            except grpc.RpcError as rpc_error:
                if rpc_error.code() == grpc.StatusCode.ABORTED:
                    logging.error(f'Inverse paraphraser has aborted while processing the request. \nrequest: ({request})')
                    context.abort(
                        code=grpc.StatusCode.ABORTED,
                        details='The input sentence may have been too long or too complex, please try calling with a simpler sentence.',
                    )
                else:
                    raise
        stylized_texts = inverse_paraphraser_response.paraphrased_texts

        if len(stylized_texts) != request.num_return_sequences:
            logging.error(f'There was at least one incomplete sentence in the returned sentences from the inverse paraphraser\'s generate_wrapper call. \nrequest: ({request})')
            context.abort(
                code=grpc.StatusCode.ABORTED,
                details='The input sentence may have been too long or too complex, please try calling with a simpler sentence.',
            )
        return TextStyleTransferResponse(stylized_texts=stylized_texts)

def serve(args):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=args.num_workers))
    add_TextStyleTransferServicer_to_server(
        TextStyleTransferServicer(args),
        server,
    )
    server.add_insecure_port(f'{args.ip}:{args.port}')
    server.start()
    print('The service has been started.')
    server.wait_for_termination()

def add_argparse_args():
    parser = ArgumentParser()
    parser.add_argument('--ip', type=str, default='[::]')
    parser.add_argument('--port', type=int, default=50051)
    parser.add_argument('--num_workers', type=int, default=1)
    parser.add_argument('--max_input_length', type=int, default=80)
    parser.add_argument('--max_num_return_sequences', type=int, default=5)

    parser.add_argument('--logging_level', type=str, default='INFO')
    parser.add_argument('--log_file_path', type=str, default=f'log/run_text_style_transfer_service.log')
    parser.add_argument('--log_file_max_bytes', type=int, default=1073741824)
    parser.add_argument('--log_file_backup_count', type=int, default=10)
    parser.add_argument('--config_file_path', type=str, default=f'proto/text_style_transfer_gateway_config.yaml')
    return parser

if __name__ == '__main__':
    parser = add_argparse_args()
    args = parser.parse_args()
    utils.set_logger(
        logging_file_path=args.log_file_path,
        logging_level=args.logging_level,
        max_bytes=args.log_file_max_bytes,
        backup_count=args.log_file_backup_count
    )
    serve(args)