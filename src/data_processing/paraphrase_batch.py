import torch

class EncoderDecoderParaphraseBatch:

    def __init__(
        self,
        encoder_input_ids=None, 
        decoder_input_ids=None, 
        encoder_attention_mask=None, 
        labels=None,
        src_str=None,
        tgt_str=None,
    ):
        self.encoder_input_ids = encoder_input_ids
        self.decoder_input_ids = decoder_input_ids
        self.encoder_attention_mask = encoder_attention_mask
        self.labels = labels
        self.src_str = src_str
        self.tgt_str = tgt_str
    
    def __len__(self) -> int:
        """Returns the length of the encoder input_ids.

        Returns:
            [int]: The sequence length of the encoder input ids.
        """
        return self.encoder_input_ids.size(0)
    
    def to(self, device):
        """Moves the tensors to the specified device.

        Args:
            device ([type]): The device to which the tensors will be moved.

        Returns:
            [EncoderDecoderParaphraseBatch]: It returns itself. Moved tensors can be accessed through the returned instance.
        """
        self.encoder_input_ids = self.encoder_input_ids.to(device) if self.encoder_input_ids is not None else None
        self.decoder_input_ids = self.decoder_input_ids.to(device) if self.decoder_input_ids is not None else None
        self.encoder_attention_mask = self.encoder_attention_mask.to(device) if self.encoder_attention_mask is not None else None
        self.labels = self.labels.to(device) if self.labels is not None else None
        return self

    def get_input_ids(self) -> torch.Tensor:
        """Returns the encoder input ids.

        Returns:
            [torch.Tensor]: Encoder input ids tensor
        """
        # TODO: use @property
        return self.encoder_input_ids 

class DecoderOnlyParaphraseBatch:

    def __init__(self,
        input_ids=None, 
        labels=None,
        attention_mask=None,
        src_str=None,
        tgt_str=None,
    ):
        self.input_ids = input_ids
        self.labels = labels
        self.attention_mask = attention_mask
        self.src_str = src_str
        self.tgt_str = tgt_str
    
    def __len__(self) -> int:
        """Returns the length of the input_ids.

        Returns:
            [int]: The sequence length of the input ids.
        """
        return self.input_ids.size(0)
    
    def to(self, device):
        """Moves the tensors to the specified device.

        Args:
            device ([type]): The device to which the tensors will be moved.

        Returns:
            [EncoderDecoderParaphraseBatch]: It returns itself. Moved tensors can be accessed through the returned instance.
        """
        self.input_ids = self.input_ids.to(device) if self.input_ids is not None else None
        self.labels = self.labels.to(device) if self.labels is not None else None
        self.attention_mask = self.attention_mask.to(device) if self.attention_mask is not None else None
        return self
    
    def get_input_ids(self):
        """Returns the input ids.

        Returns:
            [torch.Tensor]: Encoder input ids tensor
        """
        return self.input_ids 