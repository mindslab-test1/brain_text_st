from abc import abstractmethod, ABC
from typing import List
import torch
from transformers import BertTokenizer

class BaseTokenizerInterface(ABC):

    @abstractmethod
    def encode(self, text: str) -> List[int]:
        """
        Convert a string to token ids.

        Args:
            text (str): A string to convert.

        Returns:
            List[int]: Encoded list of ints.
        """
        pass
    
    @abstractmethod
    def decode(self, ids: List[int]) -> str:
        """
        Convert list of token ids to a string.

        Args:
            ids (List[int]): A list of token ids.

        Returns:
            str: Decoded string.
        """
        pass 

    @abstractmethod
    def tokenize(self, text: str) -> List[str]:
        """
        Tokenize a string to tokens.

        Args:
            text (str): Input string to tokenize.

        Returns:
            List[str]: A list of tokens.
        """
        pass 

    @property 
    # TODO: Is it okay?
    def unk_token_id(self):
        """
        Returns the reserved unknown token's id for the tokenizer.

        Returns:
            [int]: Unknown token's id.
        """
        return self._unk_token_id

class HuggingFaceBertTokenizerWrapper(BaseTokenizerInterface):
    
    def __init__(self, tokenizer: BertTokenizer):
        """
        Initialize HuggingFace BertTokenizer wrapper.
        Args:
            tokenizer (BertTokenizer): HuggingFace BertTokenizer instance.
        """
        self.bert_tokenizer = tokenizer
        self._unk_token_id = self.bert_tokenizer.unk_token_id
    
    def encode(self, text: str):
        encoded = self.bert_tokenizer.encode(
            text=text,
            add_special_tokens=False,
        )
        return encoded
    
    def decode(self, ids: List[int]):
        decoded = self.bert_tokenizer.decode(
            token_ids=ids,
            skip_special_tokens=True,
            clean_up_tokenization_spaces=True,
        )
        return decoded
    
    def tokenize(self, text: str):
        return self.bert_tokenizer.tokenize(text)
    
    def ids_to_tokens(self, ids: List[int]):
        return self.bert_tokenizer.convert_ids_to_tokens(ids)

class SentencePieceWrapper(BaseTokenizerInterface):
    
    def __init__(self, tokenizer):
        """
        Initialize sentencepeice wrapper.

        Args:
            tokenizer (SentencePieceProcessor): A SentencePieceProcessor.
        """
        self.sentence_piece_model = tokenizer
        self._unk_token_id = self.sentence_piece_model.unk_id()
    
    def encode(self, text: str):
        ids = self.sentence_piece_model.encode(text)
        return ids
    
    def decode(self, ids: List[int]):
        if torch.is_tensor(ids):
            ids = ids.tolist()    
        text = self.sentence_piece_model.decode(ids)
        return text

    def tokenize(self, text: str):
        return self.sentence_piece_model.encode_as_pieces(text)
    
    def ids_to_tokens(self, ids: List[int]):
        if torch.is_tensor(ids):
            ids = ids.tolist()    
        return self.sentence_piece_model.id_to_piece(ids)