from abc import abstractmethod, ABC
from typing import List
import torch
import random
from torch.nn.utils.rnn import pad_sequence
from data_processing.paraphrase_batch import EncoderDecoderParaphraseBatch, DecoderOnlyParaphraseBatch
from data_processing.tokenizers import BaseTokenizerInterface
import utils.utils as utils

class BaseBatchProcessorInterface(ABC):
    
    @abstractmethod
    def make_batch_for_generation(self, ids_list: List[List[int]]):
        pass

    @abstractmethod
    def make_batch_for_training(
        self, 
        src_ids_list: List[torch.Tensor], 
        tgt_ids_list: List[torch.Tensor], 
        shuffle=False
    ):
        pass

    @abstractmethod
    def encode_src_texts(self, texts: List[str]) -> torch.Tensor:
        pass

    @abstractmethod
    def encode_tgt_text(self, tgt_text: str) -> torch.Tensor:
        pass

    # TODO: specify the reasons that inheritance should not be used.
    @abstractmethod
    def encode(self, text: str) -> List[int]:
        """Convert a string to token ids.

        Args:
            text (str): A string to encode.

        Returns:
            List[int]: Encoded list of ints.
        """
        pass
    
    # TODO: specify the reasons that inheritance should not be used.
    @abstractmethod
    def decode(self, ids: List[int]) -> str:
        """Convert list of token ids to a string.

        Args:
            ids (List[int]): A list of token ids.

        Returns:
            str: Decoded string.
        """
        pass

class EncoderDecoderBatchProcessor(BaseBatchProcessorInterface):
    def __init__(
        self,
        first_token_id: int,
        last_token_id: int,
        intermediate_token_id: int,
        pad_token_id: int,
        tokenizer: BaseTokenizerInterface,
        encoder_max_token_length: int=None,
        decoder_max_token_length: int=None,
    ):
        """
        Initialize EncoderDecoderBatchProcessor with the given reserved tokens and a tokenizer.

        Args:
            first_token_id (int): Token id to insert at the start of the encoder and decoder sequences when making a batch.
            last_token_id (int): Token id to append at the end of the encoder and decoder sequences when making a batch.
            intermediate_token_id (int): Token id to use when marking sentence boundaries for the inverse paraphraser.
            pad_token_id (int): Padding token.
            tokenizer (BaseTokenizerInterface): A tokenizer that supports BaseTokenizerInterface's interface.
            encoder_max_token_length ([int], optional): The maximum number of tokens the encoder can accept. (Used when building datasets)
            decoder_max_token_length ([int], optional): The maximum number of tokens the decoder can accept. (Used when building datasets)
        """
        self.first_token_id = first_token_id
        self.last_token_id = last_token_id
        self.intermediate_token_id = intermediate_token_id
        self.pad_token_id = pad_token_id
        self.tokenizer = tokenizer
        self.encoder_max_token_length=encoder_max_token_length
        self.decoder_max_token_length=decoder_max_token_length

    def encode_tgt_text(self, tgt_text: str) -> torch.Tensor:
        """Encodes the target string to its token ids.
        first_token_id is inserted at the start.
        last_token_id is appended at the end.

        Args:
            tgt_text (str): Target string to be encoded.

        Returns:
            torch.Tensor: torch.Tensor of size BxK where B is the batch size and K is the sequence length.
        """
        tgt_ids = torch.cat(
            (
                torch.tensor(self.first_token_id, dtype=torch.long, device='cpu').view(-1),
                torch.tensor(self.tokenizer.encode(tgt_text), dtype=torch.long, device='cpu').view(-1),
                torch.tensor(self.last_token_id, dtype=torch.long, device='cpu').view(-1),
            ),
            dim=-1
        )
        return tgt_ids
    
    def encode_src_texts(self, texts: List[str]) -> torch.Tensor:
        """Encodes a list of source strings to its token ids.
        first_token_id is inserted at the start.
        intermediate_token_id is inserted at the boundaries of the sentences.
        last_token_id is appended at the end.

        Args:
            texts (List[str]): A list of strings to encode.

        Returns:
            torch.Tensor: torch.Tensor of size K where K is the sequence length of the tokens concatenated.
        """
        src_ids_list = [self.tokenizer.encode(text) for text in texts]
        src_ids = self.concat_src_ids(src_ids_list)
        return src_ids
    
    def make_batch_for_training(
        self, 
        src_ids_list: List[torch.Tensor], 
        tgt_ids_list: List[torch.Tensor], 
        shuffle=False
    ) -> EncoderDecoderParaphraseBatch:
        """Makes a batch for the given two lists of source and target ids tensors.
        Right pads the encoder, decoder sequences with pad_token_id

        Args:
            src_ids_list (List[torch.Tensor]): A list of source ids tensors. Each of the tensors has its own size K_i.
            tgt_ids_list (List[torch.Tensor]): A list of source ids tensors. Each of the tensors has its own size K_j.
            shuffle (bool, optional): Shuffles source and target if set to True. Defaults to False.

        Returns:
            EncoderDecoderParaphraseBatch: An EncoderDecoderParaphraserBatch instance.
            The batch instance contains encoder, decoder input ids, encoder attention masks, decoder labels.
        """
        if shuffle and random.random() > 0.5:
            temp = tgt_ids_list
            tgt_ids_list = src_ids_list
            src_ids_list = temp
        encoder_input_ids = pad_sequence(src_ids_list, batch_first=True, padding_value=self.pad_token_id)
        decoder_input_ids = pad_sequence(tgt_ids_list, batch_first=True, padding_value=self.pad_token_id)
        encoder_attention_mask = (encoder_input_ids != self.pad_token_id).long()
        labels = torch.where(decoder_input_ids != self.pad_token_id, decoder_input_ids, torch.tensor(-100, dtype=torch.long))

        return EncoderDecoderParaphraseBatch(
            encoder_input_ids=encoder_input_ids,
            decoder_input_ids=decoder_input_ids,
            encoder_attention_mask=encoder_attention_mask,
            labels=labels,
        )

    def make_batch_for_generation(self, ids_list: List[List[List[int]]]) -> EncoderDecoderParaphraseBatch:
        """Concatenates list of token ids while adding appropriate special tokens and padding.

        The outermost list contains a list of numericalized ids lists.
        e.g.,
        [
            (first list of ids to be concatenated:)  [ [...ids...], [...ids...], [...ids...], ... ],
            (second list of ids to be concatenated:) [ [...ids...], [...ids...], [...ids...], ... ],
            (and so on and forth...)
        ]

        Args:
            ids_list (List[List[List[int]]]): A list of list of token ids to be batched together. 

        Returns:
            EncoderDecoderParaphraseBatch: An EncoderDecoderParaphraseBatch instance containing encoder input ids and its attention masks.
        """
        concatenated_src_ids_list = []
        for ids in ids_list:
        #     # src_ids_list = [self.tokenizer.encode(text) for text in texts]
            src_ids = self.concat_src_ids(ids)
            concatenated_src_ids_list.append(src_ids)
        encoder_input_ids = pad_sequence(concatenated_src_ids_list, batch_first=True, padding_value=self.pad_token_id)
        encoder_attention_mask = (encoder_input_ids != self.pad_token_id).long()
        return EncoderDecoderParaphraseBatch(
            encoder_input_ids=encoder_input_ids,
            decoder_input_ids=None,
            encoder_attention_mask=encoder_attention_mask,
            labels=None,
        )

    def concat_src_ids(self, src_ids_list: List[List[int]]) -> torch.Tensor:
        """ A helper method. Concatenates a list of token ids lists with appropriate special tokens ids.
        fisrt_token_id is inserted at the start.
        Intermediate_token id is inserted at the boundaries of the sentences.
        last_token_ids is inserted at the end.
        Args:
            src_ids_list (List[List[int]]): A list of token ids list.

        Returns:
            torch.Tensor: A concatenated list of token ids list with appropriate special token ids inserted.
            (Size: K, where K is the resulting sequence length.)
        """
        input_ids_list = [torch.tensor(self.first_token_id, dtype=torch.long, device='cpu').view(-1)]
        # TODO: for loop may not be the way to go
        for i, line in enumerate(src_ids_list):
            if i != len(src_ids_list) - 1:
                input_ids_list.append(torch.tensor(line, dtype=torch.long, device='cpu').view(-1))
                input_ids_list.append(torch.tensor(self.intermediate_token_id, dtype=torch.long, device='cpu').view(-1))
            else:
                input_ids_list.append(torch.tensor(line, dtype=torch.long, device='cpu').view(-1))
                input_ids_list.append(torch.tensor(self.last_token_id, dtype=torch.long, device='cpu').view(-1))
        concatenated = torch.cat(input_ids_list, dim=-1)
        return concatenated

    # TODO: specify the reasons that inheritance should not be used. 
    def encode(self, text: str) -> List[int]:
        return self.tokenizer.encode(text)
    
    # TODO: specify the reasons that inheritance should not be used.
    def decode(self, ids: List[int]) -> str:
        return self.tokenizer.decode(ids)

    def is_within_max_length(self, src_tokens: torch.Tensor, tgt_tokens: torch.Tensor) -> bool:
        """
        Check if an encoder, decoder input ids pair exceeds the maximum allowed length.

        Args:
            src_tokens (torch.Tensor): input token ids for the encoder. Size: (Batch size, encoder ids sequence length)
            tgt_tokens (torch.Tensor): input token ids for the decoder. Size: (Batch size, decoder ids sequence length)

        Returns:
            [bool]: True if it does not exceed the allowed limits, False otherwise.
        Raises:
            [NotImplementedError]: if the model's length limit is not set.
        """
        if self.encoder_max_token_length is None or self.decoder_max_token_length is None:
            # TODO: Check that this is the right exceotion to raise.
            return NotImplementedError
        # bos token + eos token + src length + tgt length
        if src_tokens.size(-1) > self.encoder_max_token_length:
            return False
        elif tgt_tokens.size(-1) > self.decoder_max_token_length:
            return False
        else:
            return True

class DecoderOnlyBatchProcessor(BaseBatchProcessorInterface):
    def __init__(
        self,
        first_token_id: int,
        last_token_id: int,
        intermediate_token_id: int,
        pad_token_id: int,
        tokenizer: BaseTokenizerInterface,
        max_token_length: int=None,
    ):
        """
        Initializes EncoderDecoderBatchProcessor with the given reserved tokens and a tokenizer.

        Args:
            first_token_id (int): Token id to insert at the start of the target sequences when making a batch.
            last_token_id (int): Token id to append at the end of the target sequences when making a batch.
            intermediate_token_id (int): Token id to use when marking sentence boundaries for the inverse paraphraser.
            pad_token_id (int): Padding token.
            tokenizer (BaseTokenizerInterface): A tokenizer that supports BaseTokenizerInterface's interface.
            encoder_max_token_length ([int], optional): The maximum number of tokens the model can accept. (Not used now.)
        """
        self.first_token_id = first_token_id
        self.last_token_id = last_token_id
        self.intermediate_token_id = intermediate_token_id
        self.pad_token_id = pad_token_id
        self.tokenizer = tokenizer
        self.max_token_length = max_token_length

    def encode_tgt_text(self, tgt_text: str) -> torch.Tensor:
        """
        Encodes the target string to its token ids.
        It does not insert any special tokens.

        Args:
            tgt_text (str): Target string to be encoded.

        Returns:
            torch.Tensor: torch.Tensor of size BxK where B is the batch size and K is the sequence length.
        """
        return torch.tensor(self.tokenizer.encode(tgt_text), dtype=torch.long, device='cpu').view(-1)
    
    def encode_src_texts(self, texts: List[str]) -> torch.Tensor:
        """
        Encodes a list of source strings to their token ids.
        intermediate_token_id is inserted at the boundaries of the sentences.

        Args:
            texts (List[str]): A list of strings to encode.

        Returns:
            torch.Tensor: torch.Tensor of size K where K is the sequence length of the tokens concatenated.
        """
        src_ids_list = [self.tokenizer.encode(text) for text in texts]
        src_ids = self._concat_src_ids(src_ids_list, append_bos=False)
        return src_ids
    
    def _concat_src_ids(self, src_ids_list: List[List[int]], append_bos: bool) -> torch.Tensor:
        """
        A helper method. Concatenates a list of token ids lists with appropriate special tokens ids.
        Intermediate_token id is inserted at the boundaries of the sentences.
        last_token_ids is inserted at the end if append_bos is set to True, otherwise it is not inserted at all.

        Args:
            src_ids_list (List[List[int]]): A list of token ids list.
            append_bos (bool): If set to True, inserts last_token_ids at the end of the sequence.

        Returns:
            torch.Tensor: A concatenated list of token ids list with appropriate special token ids inserted.
            (Size: K, where K is the resulting sequence length.)
        """
        input_ids_list = []
        # TODO: for loop may not be the way to go
        for i, line in enumerate(src_ids_list):
            if i != len(src_ids_list) - 1:
                input_ids_list.append(torch.tensor(line, dtype=torch.long, device='cpu').view(-1))
                input_ids_list.append(torch.tensor(self.intermediate_token_id, dtype=torch.long, device='cpu').view(-1))
            else:
                input_ids_list.append(torch.tensor(line, dtype=torch.long, device='cpu').view(-1))
                if append_bos:
                    input_ids_list.append(torch.tensor(self.first_token_id, dtype=torch.long, device='cpu').view(-1))
            concatenated = torch.cat(input_ids_list, dim=-1)
        return concatenated
    
    def make_batch_for_training(
        self, 
        src_ids_list: List[torch.Tensor], 
        tgt_ids_list: List[torch.Tensor], 
        shuffle: bool=False,
    ) -> DecoderOnlyParaphraseBatch:
        """
        Makes a batch for the given two lists of source and target ids tensors.
        Concatenates the source and target token ids and pads the sequences with pad_token_id.
        Inserts first_token_id at the boundary point of the source and target sentences.
        Appends last_token_id at the end of the sequence.

        Args:
            src_ids_list (List[torch.Tensor]): A list of source ids tensors. Each of the tensors has its own size K_i.
            tgt_ids_list (List[torch.Tensor]): A list of source ids tensors. Each of the tensors has its own size K_j.
            shuffle (bool, optional): Shuffles source and target if set to True. Defaults to False.

        Returns:
            DecoderOnlyParaphraseBatch: A DecoderOnlyParaphraseBatch instance.
        """
        if shuffle and random.random() > 0.5:
            temp = tgt_ids_list
            tgt_ids_list = src_ids_list
            src_ids_list = temp

        concatenated_ids_list = [
            torch.cat(
                (
                    x[0],
                    torch.tensor(self.first_token_id, dtype=torch.long, device='cpu').view(-1),
                    x[1],
                    torch.tensor(self.last_token_id, dtype=torch.long, device='cpu').view(-1),
                )
            )
            for x in zip(src_ids_list, tgt_ids_list)
        ]

        input_ids = pad_sequence(concatenated_ids_list, batch_first=True, padding_value=self.pad_token_id)

        # TODO: Find a faster way to make labels tensor.
        # labels = torch.empty(input_ids.size(), dtype=torch.long).fill_(-100)
        labels = input_ids.detach().clone()
        for batch_idx, (src_ids, concatenated_ids) in enumerate(zip(src_ids_list, concatenated_ids_list)):
            # should -100 be a hyperparameter?
            labels[batch_idx, :len(src_ids)+1] = -100
            labels[batch_idx, concatenated_ids.size(-1):] = -100

        # # TODO: Find a cleaner way to write this.
        # labels = torch.where(input_ids != self.pad_token_id, input_ids, torch.tensor(-100, dtype=torch.long))
        # src_ids_len_list = list(map(len, src_ids_list))
        # for batch_idx, src_id_len in enumerate(src_ids_len_list):
        #     labels[batch_idx, :src_id_len] = -100
        # TODO: Use dict?
        return DecoderOnlyParaphraseBatch(
            input_ids=input_ids,
            labels=labels,
        )

    def make_batch_for_generation(self, ids_list: List[List[List[int]]]) -> DecoderOnlyParaphraseBatch:
        """
        Concatenates list of token ids while adding appropriate special tokens and padding.
        Left padding is performed.
        Inserts first_token_id at the boundary point of the source and target sentences.
        Appends last_token_id at the end of the sequence.
        Intermediate_token id is inserted at the boundaries of the source sentences.
        The outermost list contains a list of numericalized ids lists to be batched together.
        e.g.,
        [
            (first list of ids to be concatenated:)  [ [...ids...], [...ids...], [...ids...] ],
            (second list of ids to be concatenated:) [ [...ids...], [...ids...], [...ids...] ],
            (and so on and forth...)
        ]

        Args:
            ids_list (List[List[List[int]]]): A list of list of token ids to be batched together. 

        Returns:
            DecoderOnlyParaphraseBatch: An DecoderOnlyParaphraseBatch instance containing input ids and attention masks.
        """
        concatenated_src_ids_list = []
        for ids in ids_list:
            # src_ids_list = [self.tokenizer.encode(text) for text in texts]
            src_ids = self._concat_src_ids(ids, append_bos=True)
            concatenated_src_ids_list.append(src_ids)
        input_ids = utils.pad_left(concatenated_src_ids_list, padding_value=self.pad_token_id)
        attention_mask = (input_ids != self.pad_token_id).long()
        return DecoderOnlyParaphraseBatch(
            input_ids=input_ids,
            attention_mask=attention_mask,
            labels=None,
        )

    # TODO: specify the reasons that inheritance should not be used.
    def encode(self, text: str) -> List[int]:
        return self.tokenizer.encode(text)
    
    # TODO: specify the reasons that inheritance should not be used.
    def decode(self, ids: List[int]) -> str:
        return self.tokenizer.decode(ids)
    
    def is_within_max_length(self, src_tokens: torch.Tensor, tgt_tokens: torch.Tensor):
        """
        Checks if the length of input ids exceeds the maximum allowed length.

        Args:
            src_tokens (torch.Tensor): input token ids for the encoder. Size: (Batch size, encoder ids sequence length)
            tgt_tokens (torch.Tensor): input token ids for the decoder. Size: (Batch size, decoder ids sequence length)

        Returns:
            [bool]: True if it does not exceed the allowed limit, False otherwise.
        Raises:
            [NotImplementedError]: if the model's length limit is not set.
        """
        if self.max_token_length is None:
            # TODO: Check that this is the right exception to raise.
            return NotImplementedError
        
        # if tgt_tokens is not None and src_tokens is not None:
            # bos token + eos token + src length + tgt length
        if src_tokens.size(-1) + tgt_tokens.size(-1) + 2 > self.max_token_length:
            return False
        else:
            return True
