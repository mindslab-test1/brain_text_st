from pytorch_lightning import LightningDataModule
from argparse import ArgumentParser
from torch.utils.data import DataLoader
from data_module.paraphrase_dataset import (
    IterableStyleParaphraseDataset,
    MapStyleParaphraseDataset,
)

class ParaphraseDataModule(LightningDataModule):
    def __init__(self, args, batch_processor):
        super().__init__()
        self.batch_processor = batch_processor
        self.hparams = args

    @staticmethod 
    def add_data_module_related_args(parent_parser):
        parser = ArgumentParser(parents=[parent_parser], add_help=False)
        parser.add_argument('--train_src_file_path_list', nargs='+', default=[]) 
        parser.add_argument('--val_src_file_path_list', nargs='+', default=[]) 
        parser.add_argument('--test_src_file_path_list', nargs='+', default=[]) 
        parser.add_argument('--train_tgt_file_path', type=str) 
        parser.add_argument('--val_tgt_file_path', type=str) 
        parser.add_argument('--test_tgt_file_path', type=str) 
        parser.add_argument('--first_token_id', type=int, required=True) 
        parser.add_argument('--last_token_id', type=int, required=True) 
        parser.add_argument('--intermediate_token_id', type=int, required=True) 
        parser.add_argument('--pad_token_id', type=int, required=True) 
        parser.add_argument('--check_max_token_length', action='store_true') 
        parser.add_argument('--num_tokens_per_batch', type=int, default=512) 
        parser.add_argument('--num_workers', type=int, default=512) 
        parser.add_argument('--batch_size', type=int, default=1) 
        parser.add_argument('--dataset_style', type=str, default='map') 
        parser.add_argument('--swap_src_and_tgt', action='store_true') 
        parser.add_argument('--max_num_lines', type=int, default=None) 
        parser.add_argument('--shuffle_src_lines', action='store_true') 
        parser.add_argument('--target_filter_regex')

        return parser

    # @classmethod 
    # def from_argparse_args(cls, args,):
    #     hparams = args        

    def prepare_data(self):
        pass

    def setup(self, stage=None):
        if stage == 'fit' or stage is None:
            if getattr(self.hparams, 'dataset_style', 'map') == 'iterable':
                self.train = IterableStyleParaphraseDataset(
                    src_file_path_list=self.hparams.train_src_file_path_list,
                    tgt_file_path=self.hparams.train_tgt_file_path,
                    batch_processor=self.batch_processor,
                    num_tokens_per_batch=self.hparams.num_tokens_per_batch,
                    max_num_lines=getattr(self.hparams, 'max_num_lines', None),
                    check_max_token_length=self.hparams.check_max_token_length,
                )
                self.val = IterableStyleParaphraseDataset(
                    src_file_path_list=self.hparams.val_src_file_path_list,
                    tgt_file_path=self.hparams.val_tgt_file_path,
                    batch_processor=self.batch_processor,
                    num_tokens_per_batch=self.hparams.num_tokens_per_batch,
                    max_num_lines=getattr(self.hparams, 'max_num_lines', None),
                    check_max_token_length=self.hparams.check_max_token_length,
                )
            else:
                self.train = MapStyleParaphraseDataset(
                    src_file_path_list=self.hparams.train_src_file_path_list,
                    tgt_file_path=self.hparams.train_tgt_file_path,
                    batch_processor=self.batch_processor,
                    max_num_lines=getattr(self.hparams, 'max_num_lines', None),
                    check_max_token_length=self.hparams.check_max_token_length,
                    shuffle_src_lines=self.hparams.shuffle_src_lines,
                    target_filter_regex=self.hparams.target_filter_regex,
                )
                self.val = MapStyleParaphraseDataset(
                    src_file_path_list=self.hparams.val_src_file_path_list,
                    tgt_file_path=self.hparams.val_tgt_file_path,
                    batch_processor=self.batch_processor,
                    max_num_lines=getattr(self.hparams, 'max_num_lines', None),
                    check_max_token_length=self.hparams.check_max_token_length,
                    target_filter_regex=self.hparams.target_filter_regex,
                )
                
        elif stage == 'test' or stage is None:
            if getattr(self.hparams, 'dataset_style', 'map') == 'iterable':
                self.test = IterableStyleParaphraseDataset(
                    src_file_path_list=self.hparams.test_src_file_path_list,
                    tgt_file_path=self.hparams.test_tgt_file_path,
                    batch_processor=self.batch_processor,
                    max_num_lines=getattr(self.hparams, 'max_num_lines', None),
                    check_max_token_length=self.hparams.check_max_token_length,
                )
            else:
                self.test = MapStyleParaphraseDataset(
                    src_file_path_list=self.hparams.test_src_file_path_list,
                    tgt_file_path=self.hparams.test_tgt_file_path,
                    batch_processor=self.batch_processor,
                    max_num_lines=getattr(self.hparams, 'max_num_lines', None),
                    encode=False,
                    check_max_token_length=self.hparams.check_max_token_length,
                )
    
    def train_dataloader(self):
        if self.hparams.shuffle_src_lines:
            self.train = MapStyleParaphraseDataset(
                src_file_path_list=self.hparams.train_src_file_path_list,
                tgt_file_path=self.hparams.train_tgt_file_path,
                batch_processor=self.batch_processor,
                max_num_lines=getattr(self.hparams, 'max_num_lines', None),
                check_max_token_length=self.hparams.check_max_token_length,
                shuffle_src_lines=self.hparams.shuffle_src_lines,
            )
            
        return DataLoader(
            dataset=self.train,
            collate_fn=lambda batch: self.batch_processor.make_batch_for_training(
                *list(zip(*(batch))),
                shuffle=self.hparams.swap_src_and_tgt,
            ),
            num_workers= 1 if self.hparams.dataset_style else self.hparams.num_workers,
            batch_size= None if self.hparams.dataset_style == 'iterable' else self.hparams.batch_size,
        )
    
    def val_dataloader(self):
        return DataLoader(
            dataset=self.val,
            collate_fn=lambda batch: self.batch_processor.make_batch_for_training(
                *list(zip(*(batch))),
                shuffle=self.hparams.swap_src_and_tgt,
            ),
            num_workers= 1 if self.hparams.dataset_style else self.hparams.num_workers,
            batch_size= None if self.hparams.dataset_style == 'iterable' else self.hparams.batch_size,
        )
    
    def test_dataloader(self):
        return DataLoader(
            dataset=self.test,
            collate_fn=lambda batch: batch[0],
            num_workers=getattr(self.hparams, 'num_workers', 1),
            batch_size=1,
        )