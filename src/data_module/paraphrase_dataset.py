from torch.utils.data import (
    Dataset,
    IterableDataset,
)
import torch, random, re
from torch.nn.utils.rnn import pad_sequence
from contextlib import ExitStack
from pprint import pprint
from tqdm import tqdm
from utils.utils import get_num_lines
from typing import List, Optional
from data_processing.batch_processors import BaseBatchProcessorInterface

class MapStyleParaphraseDataset(Dataset):
    
    def __init__(
        self,
        src_file_path_list: List[str],
        tgt_file_path: str,
        batch_processor: BaseBatchProcessorInterface,
        encode: bool = True,
        max_num_lines: int = None,
        check_max_token_length: bool = False,
        shuffle_src_lines: bool = False,
        target_filter_regex: Optional[str] = None
        # max_src_token_len,
        # max_tgt_token_len,
        # max_combined_token_len,
    ):
        """
        Reads lines from files and converts them to token ids using batch_processor if encode is set to True.
        Source lines are concatenated if multiple source files are provided.

        Args:
            src_file_path_list (List[str]): Source files paths
            tgt_file_path (str): Target file path
            batch_processor (BaseBatchProcessorInterface): An object that implements BaseBatchProcessorInterface.
            encode (bool, optional): Encode read lines using batch_processor. Encode Defaults to True.
            max_num_lines (int, optional): Maximum number of lines to read. Reads the entire lines if it is set to None. Defaults to None.
            check_max_token_length (bool, optional): Do not load lines that exceed the length limit set by batch_processor. Defaults to False.
            shuffle_src_lines (bool, optional): Shuffle source lines.
            target_filter_regex (str, optional): Filter by the provided pattern if there is no match when re.search is performed.
        """
        super().__init__()

        self.src_file_path_list=src_file_path_list
        self.tgt_file_path=tgt_file_path
        self.batch_processor = batch_processor
        self.encode = encode
        self.max_num_lines = max_num_lines
        self.check_max_token_length = check_max_token_length
        self.shuffle_src_lines = shuffle_src_lines
        self.target_filter_regex = target_filter_regex
        # self.max_src_token_len = max_src_token_len
        # self.max_tgt_token_len = max_tgt_token_len

        total_num_lines = get_num_lines(self.tgt_file_path)
        pbar = tqdm(
            desc=f'Loading dataset: {self.tgt_file_path}', 
            total=total_num_lines if self.max_num_lines is None else self.max_num_lines
        )

        self.src_lines = []
        self.tgt_lines = []
        cur_num_lines = 0
        max_token_exceeded_count = 0
        filtered_tgt_count = 0
        with open(self.tgt_file_path) as tgt_file, ExitStack() as stack:

            src_files = [stack.enter_context(open(src_file, 'r')) for src_file in self.src_file_path_list]

            for lines in zip(*src_files, tgt_file):

                src_lines = lines[:-1] 
                tgt_line = lines[-1]

                if self.target_filter_regex is not None and re.search(self.target_filter_regex, tgt_line) is None:
                    filtered_tgt_count += 1
                    pbar.update(1)
                    pbar.set_postfix({'filtered_tgt_count': filtered_tgt_count})
                    continue

                if self.shuffle_src_lines:
                    random.shuffle(list(src_lines)) 
                
                if encode:
                    src_tokens = self.batch_processor.encode_src_texts(src_lines)
                    tgt_tokens = self.batch_processor.encode_tgt_text(tgt_line)

                    if self.check_max_token_length and not self.batch_processor.is_within_max_length(src_tokens, tgt_tokens):
                        print('Exceeded max token length.')
                        print(f'src_lines: {src_lines}')
                        print(f'tgt_line: {tgt_line}')
                        max_token_exceeded_count += 1
                        pbar.update(1)
                        pbar.set_postfix({'num_max_token_exeeded': max_token_exceeded_count})
                        continue
                    self.src_lines.append(src_tokens)
                    self.tgt_lines.append(tgt_tokens)
                else:
                    self.src_lines.append(src_lines)
                    self.tgt_lines.append(tgt_line)

                # if src_tokens.size(-1) + tgt_tokens.size(-1) > self.max_token_len:
                #     continue
                cur_num_lines += 1
                pbar.update(1)
                if self.max_num_lines is not None and cur_num_lines >= self.max_num_lines:
                    break
        pbar.close() 
        assert len(self.src_lines) == len(self.tgt_lines), 'source and target lengths are different.'
    
    def __len__(self):
        return len(self.src_lines)
    
    def __getitem__(self, idx):
        return (
            self.src_lines[idx],
            self.tgt_lines[idx],
        )

class IterableStyleParaphraseDataset(IterableDataset):
    """
    DO NOT USE. THE FUCNTIONALITIES MAY NOT BE UP TO DATE. HAVE NOT TESTED.
    """
    
    def __init__(
        self,
        src_file_path_list,
        tgt_file_path,
        batch_processor,
        num_tokens_per_batch,
        encode=True,
        max_num_lines=None,
        # max_src_token_len,
        # max_tgt_token_len,
        # max_combined_token_len,
    ):
        """
        DO NOT USE. THE FUCNTIONALITIES MAY NOT BE UP TO DATE. HAVE NOT TESTED.
        """
        super().__init__()

        self.src_file_path_list=src_file_path_list
        self.tgt_file_path=tgt_file_path
        self.batch_processor = batch_processor
        # self.max_src_token_len = max_src_token_len
        # self.max_tgt_token_len = max_tgt_token_len
        self.num_tokens_per_batch = num_tokens_per_batch
    
    def __iter__(self):

        with open(self.tgt_file_path) as tgt_file, ExitStack() as stack:

            src_files = [stack.enter_context(open(src_file, 'r')) for src_file in self.src_file_path_list]

            src_tgt_pairs = []
            cumulative_num_tokens = 0
            src_tgt_pair = None

            for lines in zip(*src_files, tgt_file):

                src_lines = lines[:-1] 
                tgt_line = lines[-1]

                src_tokens = self.batch_processor.encode_src_texts(src_lines)
                tgt_tokens = self.batch_processor.encode_tgt_text(tgt_line)

                num_tokens = src_tokens.size(-1) + tgt_tokens.size(-1) 

                # if num_tokens > self.max_token_len:
                #     continue

                if cumulative_num_tokens + num_tokens < self.num_tokens_per_batch:
                    src_tgt_pairs.append(src_tgt_pair)
                    cumulative_num_tokens += num_tokens
                else:
                    yield src_tgt_pairs
                    src_tgt_pairs = [src_tgt_pair]
                    cumulative_num_tokens = num_tokens

            if len(src_tgt_pairs) != 0:
                yield src_tgt_pairs
    
    # def __len__(self):
    #     return self.num_lines

# TODO: consider moving this class to exceptions.py
class ParaphraseMaxTokenExceededError(Exception):
    """
    This is not used.
    """

    def __init__(
        self, 
        message='Max token length exceeded', 
        encoder_input_ids_length=None, 
        decoder_input_ids_length=None,
        max_token_length=None,
    ):
        self.message = message
        self.encoder_input_ids_length = encoder_input_ids_length
        self.decoder_input_ids_length = decoder_input_ids_length
        self.max_token_length = max_token_length
        super().__init__(self.message)
    
    def __str__(self):
        return f'{self.message} max_token_length: {self.max_token_length}, encoder_input_ids_length: {self.encoder_input_ids_length}, decoder_input_ids_length: {self.decoder_input_ids_length}'
