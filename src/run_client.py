from serving.text_style_transfer_pb2_grpc import (
    DiverseParaphraserServicer,
    DiverseParaphraserStub, 
    TextStyleTransferStub, 
    add_DiverseParaphraserServicer_to_server,
)
from serving.text_style_transfer_pb2 import (
    TextStyleTransferRequest, 
    TextStyleTransferResponse,
    DiverseParaphraserRequest,
    InverseParaphraserRequest,
    DiverseParaphraserResponse,
)
import grpc, yaml
from argparse import ArgumentParser

def call_diverse_paraphraser(args, original_text, num_return_sequences):
    with grpc.insecure_channel(
        f'{args.ip}:{args.port}'
    ) as channel:
        # print(f'Connected to {args.ip}:{args.port}')
        diverse_paraphraser_request = DiverseParaphraserRequest(
            original_text=original_text,
            num_return_sequences=num_return_sequences,
        )
        diverse_paraphraser_stub = DiverseParaphraserStub(channel)
        diverse_paraphraser_response = diverse_paraphraser_stub.GenerateParaphrasedText(diverse_paraphraser_request)

    for text in diverse_paraphraser_response.paraphrased_texts:
        print(text)

def call_inverse_paraphraser(args, original_text, num_return_sequences):
    with grpc.insecure_channel(
        f'{args.ip}:{args.port}'
    ) as channel:
        # print(f'Connected to {args.ip}:{args.port}')
        inverse_paraphraser_request = InverseParaphraserRequest(
            original_text=original_text,
            num_return_sequences=num_return_sequences,
        )
        inverse_paraphraser_stub = TextStyleTransferStub(channel)
        inverse_paraphraser_response = inverse_paraphraser_stub.GenerateStylizedText(inverse_paraphraser_request)

    for text in inverse_paraphraser_response.paraphrased_texts:
        print(text)

def call_text_style_transfer(args, original_text, num_return_sequences, style):
    with grpc.insecure_channel(
        f'{args.ip}:{args.port}'
    ) as channel:
        # print(f'Connected to {args.ip}:{args.port}')
        text_style_transfer_request = TextStyleTransferRequest(
            original_text=original_text,
            num_return_sequences=num_return_sequences,
            target_style=style,
        )
        text_style_transfer_stub = TextStyleTransferStub(channel)
        text_style_transfer_response = text_style_transfer_stub.GenerateStylizedText(text_style_transfer_request)

    for text in text_style_transfer_response.stylized_texts:
        print(text)
    

def parse_argparse_args():
    parser = ArgumentParser()
    parser.add_argument('--ip', type=str, default='114.108.173.120')
    parser.add_argument('--port', type=int, default=18290)
    parser.add_argument('--diverse', action='store_true')
    parser.add_argument('--iterate_config', action='store_true')
    parser.add_argument('--config_file_path', default='serving/text_style_transfer_gateway_config.yaml')

    # parser.add_argument('--ip', type=str, default='127.0.0.1')
    # parser.add_argument('--port', type=int, default=50052)
    return parser

def interactive(args,):
    while True:
        try:
            original_text = input('input: ')
            num_return_sequences = int(input('Number of sentences to generate: '))
            if not args.diverse:
                style = input('Style: ')
                call_text_style_transfer(args, original_text, num_return_sequences, style)
            else:
                call_diverse_paraphraser(args, original_text, num_return_sequences)
        except KeyboardInterrupt:
            break
        except Exception as e:
            print(e)
            pass

def iterate_config(args):
    with open(args.config_file_path, 'r') as f:
        config = yaml.safe_load(f)
    try:
        original_text = input('input: ')
        num_return_sequences = int(input('Number of sentences to generate: '))
        for style in config:
            print(style)
            call_text_style_transfer(args, original_text, num_return_sequences, style)
            print()
    except Exception as e:
        print(e)
        pass
    

if __name__ == '__main__':
    parser = parse_argparse_args()
    args = parser.parse_args()
    if args.iterate_config:
        iterate_config(args)
    else:
        interactive(args)
    # print('Diverse')
    # interactive(args, inverse=False)
