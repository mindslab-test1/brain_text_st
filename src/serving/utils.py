import logging 
from logging.handlers import RotatingFileHandler

def set_logger(logging_file_path, logging_level, max_bytes=1073741824, backup_count=10):
    logging.basicConfig(
        handlers=[RotatingFileHandler(logging_file_path, maxBytes=max_bytes, backupCount=backup_count, mode='a')],
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        level=getattr(logging, logging_level),
    )