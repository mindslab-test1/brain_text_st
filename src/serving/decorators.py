from functools import wraps
import grpc
import logging

# TODO: Consider using interceptors instead of custom decorators.

LOGGER = logging.getLogger(__name__)

# TODO: Write a separate validation for each request.
def validate_input_length_with(attribute):
    def validate_input_length(function):
        @wraps(function)
        def wrapper_validate_input_length(self, request, context, *args, **kwargs):
            # TODO: Do not use self.args
            if len(request.original_text) > getattr(self.args, attribute):
                message = f'Number of characters in the input string must be less then or equal to {self.args.max_input_length}. Input length: {len(request.original_text)}'
                LOGGER.info(f'{message}, {request}')
                context.abort(
                    code=grpc.StatusCode.INVALID_ARGUMENT,
                    details=message,
                )
            return function(self, request, context, *args, **kwargs)
        return wrapper_validate_input_length
    return validate_input_length

# TODO: Write a separate validation for each request.
def validate_num_return_sequences_with(attribute):
    def validate_num_return_sequences(function):
        @wraps(function)
        def wrapper_validate_num_return_sequences(self, request, context, *args, **kwargs):
            # TODO: Do not use self.args
            if request.num_return_sequences > getattr(self.args, attribute):
                message = f'Number of requested sequences ({request.num_return_sequences}) exceeds the maximun number configured ({self.args.max_num_return_sequences}).'
                LOGGER.info(f'{message}, {request}')
                context.abort(
                    code=grpc.StatusCode.INVALID_ARGUMENT,
                    details=message,
                )
            return function(self, request, context, *args, **kwargs)
        return wrapper_validate_num_return_sequences
    return validate_num_return_sequences

def log_exceptions(logger):
    def logging_decorator(function):
        @wraps(function)
        def wrapper(self, request, context, *args, **kwargs):
            try:
                return function(self, request, context, *args, **kwargs)
            except Exception as e:
                logger.exception(e)
                raise
        return wrapper
    return logging_decorator

# Consider adding a "stringifier".
def log_remote_calls(logger):
    def logging_decorator(function):
        @wraps(function)
        def wrapper(self, request, context, *args, **kwargs):
            logger.info(request)
            result = function(self, request, context, *args, **kwargs)
            logger.info(result)
            return result
        return wrapper
    return logging_decorator

def validate_text_style_transfer_requeset(function):
    @wraps(function)
    def wrapper(self, request, context, *args, **kwargs):
        if request.target_style not in self.config:
            context.abort(
                code=grpc.StatusCode.INVALID_ARGUMENT,
                details=f'Invalid argument for target_style: {request.target_style}.'
            )
        elif len(request.original_text) > self.args.max_input_length:
            context.abort(
                code=grpc.StatusCode.INVALID_ARGUMENT,
                details=f'Maximum length of the input is {self.args.max_input_length}, but the input string length was {len(request.original_text)}.'
            )
        elif request.num_return_sequences  < 1 or request.num_return_sequences > self.args.max_num_return_sequences:
            context.abort(
                code=grpc.StatusCode.INVALID_ARGUMENT,
                details=f'Maximum number of sequences that can be generated is {self.args.max_num_return_sequences}, but {request.num_return_sequences} is given as an argument.'
            )
        return function(self, request, context, *args, **kwargs)
    return wrapper