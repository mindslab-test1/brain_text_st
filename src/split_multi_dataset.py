from sklearn.model_selection import train_test_split
import sentencepiece as spm
import argparse, os
import pathlib
import logging
import collections
import sys
from contextlib import ExitStack
from pprint import (
    pformat,
)
from transformers import BertTokenizer
from data_processing.tokenizers import (
    SentencePieceWrapper,
    HuggingFaceBertTokenizerWrapper, 
)

def write_lines(path, lines):
    with open(path, 'w') as f:
        for line in lines:
            f.write(line) 

def write_datasets(dest_dir, prefix, train, valid, test=None):
    write_lines(os.path.join(dest_dir, prefix + '_train.txt'), train)
    write_lines(os.path.join(dest_dir, prefix + '_val.txt'), valid)
    if test is not None:
        write_lines(os.path.join(dest_dir, prefix + '_test.txt'), test)

def save_datasets(file_path, train, valid, test=None):
    path_obj =  pathlib.Path(file_path)
    prefix = path_obj.stem
    dest_dir = path_obj.parent
    if test is not None:
        write_datasets(dest_dir, prefix, train, valid, test)
    else:
        write_datasets(dest_dir, prefix, train, valid)

def save_list_of_datasets(file_paths, lines_list, suffix):
    with ExitStack() as stack:
        files = []
        for file_path in file_paths:
            path_obj = pathlib.Path(file_path)
            prefix = path_obj.stem
            dest_dir = path_obj.parent
            files.append(stack.enter_context(open(dest_dir / (prefix + suffix), 'w')))
        for lines in lines_list:
            for f, line in zip(files, lines):
                f.write(line)

def filter_datasets(src_lines, tgt_lines, tokenizer, args):
    filtered_src = []
    filtered_tgt = []
    filtered_cnt = 0
    num_lines = 0
    num_filtered_by_len = 0
    num_filtered_by_equality = 0
    num_filtered_by_unk = 0
    num_filtered_by_repetition = 0

    unk_token_id = tokenizer.unk_token_id
    for src_lines, tgt_line in zip(src_lines, tgt_lines):

        tgt_line_tokenized = tokenizer.encode(tgt_line)

        # TODO: should_be_filtered may have been a better name for it
        should_not_be_added = False

        for src_line in src_lines:
            src_line_tokenized = tokenizer.encode(src_line)

            if len(tgt_line_tokenized) == 0 or \
                len(src_line_tokenized) == 0 or \
                2 * len(src_line) < len(tgt_line) or \
                2 * len(tgt_line) < len(src_line):
                num_filtered_by_len += 1
                should_not_be_added = True
                logging.info(
                    f'''Filtered by length:
                    src_line: {src_line}
                    tgt_line {tgt_line}
                    line # {num_lines + filtered_cnt + 1} 
                ''')

            if len(src_line_tokenized) > 0:
                src_line_counter_dict = collections.Counter(src_line_tokenized)
                src_max_token_cnt = max(src_line_counter_dict, key=lambda key: src_line_counter_dict[key])
                src_repetition_ratio = src_line_counter_dict[src_max_token_cnt] / len(src_line_tokenized)

                if len(src_line_tokenized) > args.repetition_min_length and \
                    src_repetition_ratio > args.repetition_threshold:
                    num_filtered_by_repetition += 1
                    should_not_be_added = True
                    logging.info(
                        f'''Filtered by repetition:
                        src_line: {src_line.strip()} 
                        tgt_line {tgt_line.strip()}
                        line # {num_lines + filtered_cnt + 1}
                        src_repetition_ratio: {src_repetition_ratio}
                        src_toknized length: {len(src_line_tokenized)}
                    ''')

            # Consider excluding unk tokens when sampling.
            if unk_token_id in src_line_tokenized or unk_token_id in tgt_line_tokenized:
                num_filtered_by_unk += 1
                should_not_be_added = True
                logging.info(
                    f'''Filtered by unk token:
                    src_line: {src_line} 
                    tgt_line {tgt_line} 
                    line # {num_lines + filtered_cnt + 1} 
                    ''')

            if src_line == tgt_line:
                num_filtered_by_equality += 1
                should_not_be_added = True
                logging.info(
                    f'''Filtered by equality:
                    src_line: {src_line} 
                    tgt_line {tgt_line}
                    line # {num_lines + filtered_cnt + 1}                
                    ''')

        if not should_not_be_added:
            filtered_src.append(src_lines)
            filtered_tgt.append(tgt_line)
            num_lines += 1
        else:
            filtered_cnt += 1
        
        if args.max_num_lines is not None and num_lines >= args.max_num_lines:
            logging.info(f'Reached max_num_lines. num_lines: {num_lines} | max_num_lines: {args.max_num_lines} ')
            break
    
    # TODO: Usze handler
    for io_function in [print, logging.info]:
        io_function(f'# of lines filtered by repetition: {num_filtered_by_repetition}')
        io_function(f'# of lines filtered by relative length: {num_filtered_by_len}')
        io_function(f'# of lines filtered by equality: {num_filtered_by_equality}')
        io_function(f'# of lines filtered by unknown token: {num_filtered_by_unk}')
        io_function(f'# of lines before filtering: {num_lines + filtered_cnt}')
        io_function(f'# of lines after filtering: {num_lines}')
        io_function(f'# of lines filtered: {filtered_cnt}')
        io_function(f'% of lines filtered: {filtered_cnt / (num_lines + filtered_cnt)}')

    assert len(filtered_src) == len(filtered_tgt)

    return filtered_src, filtered_tgt

def read_datasets(src_file_paths, tgt_file_path):
    num_lines = 0
    src = []
    tgt = []

    with \
        ExitStack() as stack, \
        open(tgt_file_path, 'r') as tgt_lines:

        src_files = [
            stack.enter_context(open(src_file_path, 'r')) for src_file_path in src_file_paths
        ]

        for lines in zip(*src_files, tgt_lines):
            src_lines = lines[:-1]
            tgt_line = lines[-1]

            src.append(src_lines)
            tgt.append(tgt_line)
            num_lines += 1
        return src, tgt

    for io_function in [print, logging.info]:
        io_function(f'# of lines read: {num_lines}')
    
    assert len(src) == len(tgt)

    return src, tgt

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--src_file_path', nargs='+', required=True)
    parser.add_argument('--tgt_file_path', required=True)
    parser.add_argument('--valid_ratio', type=float, required=True)
    parser.add_argument('--test_ratio', type=float, required=False)
    parser.add_argument('--repetition_min_length', type=int, required='--do_not_filter' not in sys.argv)
    parser.add_argument('--repetition_threshold', type=float, required='--do_not_filter' not in sys.argv)
    parser.add_argument('--max_num_lines', type=int)
    parser.add_argument('--do_not_filter', action='store_true')
    parser.add_argument('--tokenizer_type', type=str)
    parser.add_argument('--tokenizer_path', type=str)
    args = parser.parse_args()

    # pprint(vars(args))
    args_dict = vars(args)
    for key, value in args_dict.items():
        print(f'{key}: {value}')
    
    if args.tokenizer_type == 'sentencepiece':
        # TODO: Modify it to load from a file.
        sp_model = spm.SentencePieceProcessor(model_file=args.tokenizer_path)
        tokenizer = SentencePieceWrapper(sp_model)
    elif args.tokenizer_type == 'bert_tokenizer':
        bert_tokenizer = BertTokenizer.from_pretrained(args.tokenizer_path)
        tokenizer = HuggingFaceBertTokenizerWrapper(bert_tokenizer)

    tgt_file_prefix = pathlib.Path(args.tgt_file_path).stem
    tgt_file_dir = pathlib.Path(args.tgt_file_path).parent
    logging.basicConfig(
        filename=tgt_file_dir / f'{tgt_file_prefix}.log',
        filemode='w',
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        level=logging.DEBUG,
    )
    logging.info('Strating...')
    logging.info('\n' + pformat(vars(args)))

    src_lines, tgt_lines = read_datasets(args.src_file_path, args.tgt_file_path)
    if not args.do_not_filter:
        src_lines, tgt_lines = filter_datasets(src_lines, tgt_lines, tokenizer, args)

    if args.test_ratio is not None:
        src_train, src_valid, tgt_train, tgt_valid = train_test_split(
            src_lines,
            tgt_lines, 
            test_size=args.valid_ratio + args.test_ratio,
            random_state=1,
        )
        src_valid, src_test, tgt_valid, tgt_test = train_test_split(
            src_valid, 
            tgt_valid, 
            test_size=args.test_ratio / (args.valid_ratio + args.test_ratio),
            random_state=1,
        )
    else:
        src_train, src_valid, tgt_train, tgt_valid = train_test_split(
            src_lines, 
            tgt_lines, 
            test_size=args.valid_ratio,
            random_state=1,
        )
    
    assert len(src_train) == len(tgt_train)
    assert len(src_valid) == len(tgt_valid)
    if args.test_ratio is not None:
        assert len(src_test) == len(tgt_test)

    save_list_of_datasets(args.src_file_path, src_train, '_train.txt')
    save_list_of_datasets(args.src_file_path, src_valid, '_val.txt')
    if args.test_ratio is not None:
        save_list_of_datasets(args.src_file_path, src_test, '_test.txt')

    # TODO: Use handler
    if args.test_ratio is not None:
        save_datasets(args.tgt_file_path, tgt_train, tgt_valid, tgt_test)
        for io_function in [print, logging.info]:
            io_function(f'src_train length: {len(src_train)}')
            io_function(f'tgt_train length: {len(tgt_train)}')
            io_function(f'src_valid length: {len(src_valid)}')
            io_function(f'tgt_valid length: {len(tgt_valid)}')
            io_function(f'src_test length: {len(src_test)}')
            io_function(f'tgt_test length: {len(tgt_test)}')
    else:
        save_datasets(args.tgt_file_path, tgt_train, tgt_valid)
        for io_function in [print, logging.info]:
            io_function(f'src_train length: {len(src_train)}')
            io_function(f'tgt_train length: {len(tgt_train)}')
            io_function(f'src_valid length: {len(src_valid)}')
            io_function(f'tgt_valid length: {len(tgt_valid)}')
