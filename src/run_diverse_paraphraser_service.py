from serving.text_style_transfer_pb2_grpc import (
    DiverseParaphraserServicer, 
    add_DiverseParaphraserServicer_to_server,
)
from serving.text_style_transfer_pb2 import (
    DiverseParaphraserRequest,
    DiverseParaphraserResponse,
)
from concurrent import futures
from argparse import ArgumentParser
import sentencepiece as spm
from encoder_decoder_paraphraser.encoder_decoder_paraphraser import (
    LitBert2BertParaphraser, 
)
from decoder_only_paraphraser.decoder_only_paraphraser import (
    LitGPT2Paraphraser
)
from data_processing.tokenizers import (
    HuggingFaceBertTokenizerWrapper,
    SentencePieceWrapper,
)
from transformers import BertTokenizer
from serving import decorators, utils
import logging, torch, grpc
# from legacy.paraphraser import LitGPT2Paraphraser as LitKoGPT2Paraphraser

class DiverseParaphraserServicer(DiverseParaphraserServicer): 
    def __init__(self, args,):
        if args.model_type == 'bert2bert':
            self.diverse_paraphraser = LitBert2BertParaphraser.load_from_checkpoint(
                checkpoint_path=args.checkpoint_path,
                map_location=torch.device(args.device),
            )
        elif args.model_type == 'gpt2':
            self.diverse_paraphraser = LitGPT2Paraphraser.load_from_checkpoint(
                checkpoint_path=args.checkpoint_path,
                map_location=torch.device(args.device),
            )
        # elif args.model_type == 'kogpt2':
        #     # TODO: Do not load the model this way.
        #     self.diverse_paraphraser = LitKoGPT2Paraphraser.load_from_checkpoint(
        #         args.checkpoint_path,
        #         map_location=args.device,
        #     )
        #     self.diverse_paraphraser.hparams['first_token_id'] = 0
        #     self.diverse_paraphraser.hparams['last_token_id'] = 1
        #     self.diverse_paraphraser.hparams['pad_token_id'] = 3
        #     self.diverse_paraphraser.hparams['intermediate_token_id'] = 2
        #     # self.inverse_paraphraser = LitGPT2Paraphraser(model.hparams)
        #     # self.inverse_paraphraser.decoder_only = model.gpt2
        else: 
            raise ValueError('Invalid model_type argument.')
            
        if args.tokenizer_type == 'bert_tokenizer':
            bert_tokenizer = BertTokenizer.from_pretrained(args.tokenizer_path)
            tokenizer = HuggingFaceBertTokenizerWrapper(bert_tokenizer)
        elif args.tokenizer_type == 'sentencepiece':
            sp_model = spm.SentencePieceProcessor(args.tokenizer_path)
            tokenizer = SentencePieceWrapper(sp_model)
        else:
            raise ValueError('Invalid tokenizer_type argument.')
        self.diverse_paraphraser.load_batch_processor(tokenizer)

        self.args = args
        self.diverse_paraphraser.to(args.device)
        self.diverse_paraphraser.eval()
        # May not be the best idea.
        print(f'Model is running on {next(self.diverse_paraphraser.parameters()).device}')

    @decorators.log_exceptions(logging)
    @decorators.log_remote_calls(logging)
    @decorators.validate_input_length_with('max_input_length')
    @decorators.validate_num_return_sequences_with('max_num_return_sequences')
    def GenerateDiverseParaphrase(self, request: DiverseParaphraserRequest, context):
        # TODO: consiter converting the strings to ids befor passing them to the model.
        with torch.no_grad():
            paraphrased_texts = self.diverse_paraphraser.generate_wrapper(
                texts=request.original_text,
                top_p=self.args.top_p,
                num_return_sequences=request.num_return_sequences,
                temperature=self.args.temperature,
                num_beams=self.args.num_beams,
                top_k=self.args.top_k,
                do_sample=self.args.do_sample,
                max_length=self.args.max_length,
                return_str=True,
                device=self.args.device,
            )
        if len(paraphrased_texts) != request.num_return_sequences:
            context.abort(
                code=grpc.StatusCode.ABORTED,
                details=f'The input sentence may have been too long or too complex, please try calling with a simpler sentence. \nrequest: ({request})',
            )
        return DiverseParaphraserResponse(paraphrased_texts=paraphrased_texts)

def serve(args):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=args.num_workers))
    add_DiverseParaphraserServicer_to_server(
        DiverseParaphraserServicer(args),
        server,
    )
    server.add_insecure_port(f'{args.ip}:{args.port}')
    server.start()
    print('The service has been started.')
    server.wait_for_termination()

def add_argparse_args():
    parser = ArgumentParser()
    parser.add_argument('--port', type=int, default=50051)
    parser.add_argument('--ip', type=str, default='[::]')
    parser.add_argument('--num_workers', type=int, default=1)
    parser.add_argument('--logging_level', type=str, default='INFO')
    parser.add_argument('--log_file_path', type=str, default=f'log/run_diverse_paraphraser_service.log')

    parser.add_argument('--model_type', type=str, default='gpt2')
    parser.add_argument('--checkpoint_path', type=str, default='checkpoints/model.ckpt')
    parser.add_argument('--tokenizer_type', type=str, default='sentencepiece')
    parser.add_argument('--tokenizer_path', type=str, default='kogpt2/kogpt2_unk_chars_added.spiece')
    parser.add_argument('--device', type=str, default='cuda:0')

    parser.add_argument('--max_input_length', type=int, default=80)
    parser.add_argument('--max_num_return_sequences', type=int, default=5)
    # Decoding related args
    parser.add_argument('--max_length', type=int, default=80)
    parser.add_argument('--top_p', type=float, default=1.0)
    parser.add_argument('--top_k', type=int, default=0)
    parser.add_argument('--temperature', type=float, default=1.0)
    parser.add_argument('--num_beams', type=int, default=5)
    parser.add_argument('--do_sample', action='store_true')

    # top-p
    # parser.add_argument('--top_p', type=float, default=0.4)
    # parser.add_argument('--top_k', type=int, default=0)
    # parser.add_argument('--temperature', type=float, default=1.0)
    # parser.add_argument('--num_beams', type=int, default=5)
    # parser.add_argument('--do_sample', action='store_false')

    return parser

def parse_args(parser):
    args = parser.parse_args()
    if not args.do_sample and args.max_num_return_sequences > args.num_beams:
        raise ValueError('max_num_return_sequences cannot be greater than num_beams when do_sample is True.')
    return args

if __name__ == '__main__':
    parser = add_argparse_args()
    args = parse_args(parser)
    utils.set_logger(args.log_file_path, args.logging_level)
    serve(args)
