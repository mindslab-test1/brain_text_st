import pytorch_lightning as pl 
from transformers import GPT2LMHeadModel, GPT2Config
import torch
from argparse import ArgumentParser
from data_processing.paraphrase_batch import DecoderOnlyParaphraseBatch
from data_processing.batch_processors import DecoderOnlyBatchProcessor
from utils.evaluators import SemanticEvaluator
from typing import List, Any
from pathlib import Path

class LitDecoderOnlyParaphraser(pl.LightningModule):

    def __init__(self, hparams):
        super().__init__()
        self.save_hyperparameters(hparams)
        if self.hparams.pretrained_path is not None:
            if self.hparams.model_type == 'gpt2':
                self.decoder_only = GPT2LMHeadModel.from_pretrained(self.hparams.pretrained_path)

                # config = GPT2Config.from_json_file(Path(self.hparams.pretrained_path) / 'config.json')
                # self.decoder_only = GPT2LMHeadModel(config)
                # self.decoder_only.lm_head = torch.nn.Linear(config.n_embd, config.vocab_size, bias=False)
                # self.decoder_only.tie_weights()
# 
    @staticmethod
    def add_argparse_args(parent_parser):
        parser = ArgumentParser(parents=[parent_parser], add_help=False)
        parser.add_argument('--learning_rate', type=float, default=5.0e-05)
        parser.add_argument('--use_segment_embedding', action='store_true') 
        parser.add_argument('--lr_decay_patience', type=int) 
        parser.add_argument('--lr_decay_factor', type=float) 
        return parser

    def training_step(self, batch, batch_idx):
        # if self.hparams.use_segment_embedding: 
        #     outputs = self(input_ids=input_ids, labels=labels, token_type_ids=token_type_ids)
        # else:
        #     outputs = self(input_ids=input_ids, labels=labels)
        outputs = self.decoder_only(
            input_ids=batch.input_ids,
            labels=batch.labels,
            return_dict=True,
        )
        loss = outputs[0]
        self.log('train_loss', loss, on_step=True, on_epoch=True, prog_bar=True, logger=True)
        self.log('train_perp', torch.exp(loss), on_step=True, on_epoch=True, prog_bar=True, logger=True)
        # self.log('input_length', input_ids.size()[1], on_step=True, logger=True)
        return loss
    
    def validation_step(self, batch, batch_idx):
        # if self.hparams.use_segment_embedding: 
        #     outputs = self(input_ids=input_ids, labels=labels, token_type_ids=token_type_ids)
        # else:
        #     outputs = self(input_ids=input_ids, labels=labels)
        outputs = self.decoder_only(
            input_ids=batch.input_ids,
            labels=batch.labels,
            return_dict=True,
        )
        loss = outputs[0]
        self.log('val_loss', loss, on_epoch=True, prog_bar=True)
        self.log('val_perp', torch.exp(loss), on_epoch=True, prog_bar=True)
    
    def configure_optimizers(self):
        optimizer = torch.optim.Adam(
            self.parameters(), 
            lr=self.hparams.learning_rate,
            betas=(0.9, 0.999),
        )
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            optimizer=optimizer,
            mode='min',
            factor=self.hparams.lr_decay_factor,
            patience=self.hparams.lr_decay_patience,
            threshold=0.0,
            threshold_mode='abs',
        )
        return {
            'optimizer': optimizer,
            'lr_scheduler': scheduler,
            'monitor': 'val_loss',
        }

    def on_test_epoch_start(self) -> None:
        self.evaluator = SemanticEvaluator()
        return self.evaluator.on_test_epoch_start(self, model_attribute='decoder_only')

    @torch.no_grad()
    def test_step(self, src_tgt_pair, batch_idx, dataloader_idx=None):
        return self.evaluator.test_step(
            self=self, 
            src_tgt_pair=src_tgt_pair, 
            model_attribute='decoder_only', 
            batch_idx=batch_idx, 
            dataloader_idx=dataloader_idx,
        )

    def test_epoch_end(self, outputs: List[Any]) -> None:
        return self.evaluator.test_epoch_end(
            self=self, 
            outputs=outputs,
            model_attribute='decoder_only'
        )

    def generate_inverse_paraphraser_input_ids(
        self,
        top_p, 
        device,
        input_ids,
        diverse_num_return_sequences,
        temperature,
        num_beams,
        top_k,
        do_sample,
        max_length,
    ):
        input_ids = torch.cat(
            (
                torch.tensor(input_ids, dtype=torch.long, device=device).view(1, -1),
                torch.tensor(self.hparams.first_token_id, dtype=torch.long, device=device).view(1, -1),
            ),
            dim=-1,
        )
        output_ids_list = []
        for i, (p, diverse_num_return_sequence) in enumerate(zip(top_p, diverse_num_return_sequences)):
            generated_tensor = self.decoder_only.generate(
                input_ids=input_ids, 
                do_sample=do_sample,
                top_p=p,
                top_k=top_k,
                max_length=max_length, 
                temperature=temperature,
                pad_token_id=self.hparams.pad_token_id,
                eos_token_id=self.hparams.last_token_id,
                num_beams=num_beams,
                num_return_sequences=diverse_num_return_sequence
            )
            # TODO: is for loop the best I can do?
            for j, generated in enumerate(torch.split(generated_tensor, 1)):
                # Remove the eos token
                # TODO: It does not have to loop twice, change this!
                if self.hparams.last_token_id in generated:
                    # print(generated)
                    eos_idx = (generated == self.hparams.last_token_id).nonzero(as_tuple=False)[0]
                    generated = generated[:, input_ids.size(1):eos_idx[-1]]
                    # print(generated)
                else:
                    generated = generated[:, input_ids.size(1):]
                # If it is the last sentence, append the first token.
                if j == generated_tensor.size(0) - 1 and i == len(top_p) - 1:
                    # normalized_idx = torch.cat([normalized_idx, generated], dim=-1)
                    output_ids_list.append(generated)
                    output_ids_list.append(torch.tensor(self.hparams.first_token_id, device=device).view(1, -1))
                # If it is not the last sentence, append intermediate token id.
                else:
                    output_ids_list.append(generated)
                    output_ids_list.append(torch.tensor(self.hparams.intermediate_token_id, device=device).view(1, -1))
        output_ids = torch.cat(output_ids_list, dim=-1)
        batch = DecoderOnlyParaphraseBatch(
            input_ids=output_ids, 
            labels=None,
        )
        return batch
    
    def generate_wrapper(
        self, 
        top_p, 
        num_return_sequences,
        temperature,
        num_beams,
        top_k,
        do_sample,
        max_length,
        return_str=False,
        batch=None,
        texts='',
        device=None,
    ):
        if batch is None:
            if type(texts) is str:
                texts = [texts]
            encoded_texts = [self.batch_processor.encode(text) for text in texts]
            batch = self.batch_processor.make_batch_for_generation([encoded_texts])
        if device is not None:
            batch = batch.to(device)
            # print(batch.attention_mask)
        # max_length = min(self.decoder_only.config.n_embd, max_length + batch.input_ids.size(-1))

        # Just use the max length the caller has provided, 
        # even if there is a chance that the value might exceed the model max length.
        # try:
        #     max_length = min(self.batch_processor.max_length, max_length + batch.input_ids.size(-1))
        # except:
        #     max_length = max_length + batch.input_ids.size(-1)
        max_length = max_length + batch.input_ids.size(-1)
        generated_tensor = self.decoder_only.generate(
            input_ids=batch.input_ids, 
            attention_mask=batch.attention_mask,
            do_sample=do_sample,
            top_p=top_p,
            top_k=top_k,
            max_length=max_length, 
            temperature=temperature,
            pad_token_id=self.hparams.pad_token_id,
            eos_token_id=self.hparams.last_token_id,
            num_beams=num_beams,
            num_return_sequences=num_return_sequences,
            decoder_start_token_id=self.hparams.first_token_id
        )
        if return_str:
            return [
                self.batch_processor.decode(ids) 
                # else self.batch_processor.decode(ids[:-1])
                for ids in generated_tensor[:, batch.input_ids.size(-1):].tolist()
                if self.hparams.last_token_id in ids
            ]
        else:
            return generated_tensor[:, batch.input_ids.size(-1):]
    
class LitGPT2Paraphraser(LitDecoderOnlyParaphraser):
    
    def __init__(self, hparams):
        super().__init__(hparams)
    
    def load_batch_processor(self, tokenizer,):
        batch_processor = DecoderOnlyBatchProcessor(
            first_token_id=self.hparams.first_token_id,
            last_token_id=self.hparams.last_token_id,
            intermediate_token_id=self.hparams.intermediate_token_id,
            pad_token_id=self.hparams.pad_token_id,
            tokenizer=tokenizer,
            max_token_length=self.decoder_only.config.n_embd,
        )
        self.batch_processor = batch_processor
        return batch_processor