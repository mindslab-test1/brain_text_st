from scipy.stats import kendalltau
import math, os, string, yaml, logging, sys, pprint
from collections import Counter
import matplotlib.pyplot as plt
import numpy as np
from argparse import ArgumentParser
from pathlib import Path
from utils.utils import get_num_lines
from tqdm import tqdm
import numpy as np

def eval_preprocess(s):
    s = s.lower()

    # Remove punctuations 
    punctuations = set(string.punctuation)
    s = ''.join([char for char in s if char not in punctuations])

    # Remove extra whitespaces
    s = ' '.join(s.split())

    return s

def kendalltau_correlation(str1, str2):
    # The code is a modification of
    # https://github.com/martiansideofthemoon/style-transfer-paraphrase/blob/master/style_paraphrase/evaluation/scripts/utils.py
    # Shared token ratio calculation is added.
    str1 = eval_preprocess(str1)
    str2 = eval_preprocess(str2)

    str1_tokens = str1.split()
    str2_tokens = str2.split()

    str1_tokens_len = len(str1_tokens)
    str2_tokens_len = len(str2_tokens)
    total_tokens_len = str1_tokens_len + str2_tokens_len

    for str1_idx, str1_token in enumerate(str1_tokens):
        try:
            str2_idx = str2_tokens.index(str1_token)
            str1_tokens[str1_idx] = f' {str1_idx + 1}'
            str2_tokens[str2_idx] = f' {str1_idx + 1}'
        except ValueError:
            pass
    
    str1_shared_tokens = [int(token) for token in str1_tokens if token.startswith(' ')]
    str2_shared_tokens = [int(token) for token in str2_tokens if token.startswith(' ')]
    
    shared_tokens_len = len(str1_shared_tokens)

    shared_tokens_ratio = shared_tokens_len / (total_tokens_len / 2)

    kd_result = kendalltau(str1_shared_tokens, str2_shared_tokens)
    correlation = kd_result.correlation

    return correlation, correlation * shared_tokens_ratio

def word_overlap(hypothesis, reference):
    hypothesis_tokens = eval_preprocess(hypothesis).split()
    reference_tokens = eval_preprocess(reference).split()
    shared_count_min = Counter(hypothesis_tokens) & Counter(reference_tokens)
    shared_count_sum = sum(shared_count_min.values())
    return shared_count_sum / len(hypothesis_tokens) 

def filter_lines(
    reference_path, 
    hypothesis_path, 
    logging_dir,
    sim_score_path,
    kendall_tau_threshold, 
    kendall_tau_norm_threshold, 
    word_overlap_threshold, 
    sim_score_threshold,
    filtered_ref_path,
    filtered_hyp_path
):
    total_num_lines = get_num_lines(reference_path) 
    with \
        open(reference_path, 'r') as refs, \
        open(hypothesis_path, 'r') as hyps, \
        open(filtered_ref_path, 'w') as filtered_refs, \
        open(filtered_hyp_path, 'w') as filtered_hyps, \
        open(sim_score_path, 'r') as sim_scores:

        word_overlap_list = []
        kd_corr_list = []
        kd_corr_norm_list = []
        sim_score_list = []
        filtered_by_word_overlap_cnt = 0
        filtered_by_kd_corr_cnt = 0
        filtered_by_kd_corr_norm_cnt = 0
        filtered_by_sim_score_cnt = 0
        total_filtered_cnt = 0
        total_num_lines = 0
        
        pbar = tqdm(desc=Path(reference_path).name, total=get_num_lines(Path(reference_path)))
        for ref_line, hyp_line, sim_score in zip(refs, hyps, sim_scores):
            word_overlap_score = word_overlap(hypothesis=hyp_line, reference=ref_line)
            word_overlap_list.append(word_overlap_score)

            kd_corr, kd_corr_norm = kendalltau_correlation(hyp_line, ref_line)
            if math.isnan(kd_corr):
                kd_corr = -1.1
                kd_corr_norm = -1.1
            kd_corr_list.append(kd_corr)
            kd_corr_norm_list.append(kd_corr_norm)

            sim_score = float(sim_score)
            sim_score_list.append(sim_score)

            filtered = False
            if word_overlap_score > word_overlap_threshold:
                filtered_by_word_overlap_cnt += 1
                filtered = True
            if kd_corr > kendall_tau_threshold:
                filtered_by_kd_corr_cnt += 1
                filtered = True
            if kd_corr_norm > kendall_tau_norm_threshold:
                filtered_by_kd_corr_norm_cnt += 1
                filtered = True
            if sim_score < sim_score_threshold:
                filtered_by_sim_score_cnt += 1
                filtered = True

            if filtered:
                total_filtered_cnt += 1
            else:
                filtered_hyps.write(hyp_line)
                filtered_refs.write(ref_line)
            total_num_lines += 1
            pbar.update(1)

    logging.info(f'# of lines filtered by word overlap: {filtered_by_word_overlap_cnt} ({100 * filtered_by_word_overlap_cnt / total_num_lines:.4f}%)')
    logging.info(f'# of lines filtered by kendall tau: {filtered_by_kd_corr_cnt} ({100 * filtered_by_kd_corr_cnt / total_num_lines:.4f}%)')
    logging.info(f'# of lines filtered by kendall tau norm: {filtered_by_kd_corr_norm_cnt} ({100 * filtered_by_kd_corr_norm_cnt / total_num_lines:.4f}%)')
    logging.info(f'# of lines filtered by similarity score: {filtered_by_sim_score_cnt} ({100 * filtered_by_sim_score_cnt / total_num_lines:.4f}%)')
    logging.info(f'Total # lines filtered: {total_filtered_cnt} ({100 * total_filtered_cnt / total_num_lines:.4f}%)')
    logging.info(f'Total # lines: {total_num_lines}')
    
    hist_dict = {
        'word_overlap': word_overlap_list,
        'kd_corr': kd_corr_list,
        'kd_corr_norm': kd_corr_norm_list,
        'sim_score': sim_score_list
    }
    for i, (title, values) in enumerate(hist_dict.items()):
        plt.figure(i)
        bins = np.linspace(min(values), max(values), 100)
        plt.hist(values, density=True, bins=bins)
        plt.title(title)
        plt.savefig(os.path.join(logging_dir, f'{title}.png'))
    
def parse_args():
    parser = ArgumentParser()
    parser.add_argument('--reference_path', type=str)
    parser.add_argument('--hypothesis_path', type=str)
    parser.add_argument('--filtered_ref_path', type=str)
    parser.add_argument('--filtered_hyp_path', type=str)
    parser.add_argument('--logging_dir', type=str)
    parser.add_argument('--sim_score_path', type=str)
    parser.add_argument('--kendall_tau_threshold', type=float)
    parser.add_argument('--kendall_tau_norm_threshold', type=float)
    parser.add_argument('--word_overlap_threshold', type=float)
    parser.add_argument('--sim_score_threshold', type=float)
    parser.add_argument('--shuffled_sim_score_path', type=str)
    parser.add_argument('--shuffled_percentile', type=int)
    args = parser.parse_args()
    return args

def calculate_percentile_from_a_file(file_path, percentile):
    scores = [] 
    with open(Path(file_path), 'r') as f:
        for score in f:
            score = float(score) 
            scores.append(score) 
        # avg = sum(scores) / len(scores)
        scores = np.array(scores)
        return np.percentile(scores, percentile)
        # return max(scores)

def set_logger(args):
    logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        level=logging.INFO,
        handlers= [
            logging.FileHandler(
                Path(args.logging_dir) / 'paraphrase_filtering.log',
                mode='w',
            ),
            logging.StreamHandler(sys.stdout),
        ]
    )

if __name__ == '__main__':
    args = parse_args()

    set_logger(args)

    if args.shuffled_sim_score_path is not None:
        shuffled_percentile = calculate_percentile_from_a_file(args.shuffled_sim_score_path, args.shuffled_percentile) 
        print(f'Setting sim_score_threshold to {shuffled_percentile}')
        args.sim_score_threshold = float(shuffled_percentile)

    logging.info(pprint.pformat(vars(args), indent=4))
    
    with open(Path(args.logging_dir) / 'filtering_parameters.yaml', 'w') as f:
        yaml.dump(vars(args), f, default_flow_style=False)

    print('Filtering data...')
    filter_lines(
        reference_path=args.reference_path,
        hypothesis_path=args.hypothesis_path,
        filtered_ref_path=args.filtered_ref_path,
        filtered_hyp_path=args.filtered_hyp_path,
        logging_dir=args.logging_dir,
        sim_score_path=args.sim_score_path,
        kendall_tau_threshold=args.kendall_tau_threshold,
        kendall_tau_norm_threshold=args.kendall_tau_norm_threshold,
        word_overlap_threshold=args.word_overlap_threshold,
        sim_score_threshold=args.sim_score_threshold,
    )