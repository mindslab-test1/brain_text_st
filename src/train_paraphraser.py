from data_module.paraphrase_data_module import ParaphraseDataModule
from encoder_decoder_paraphraser.encoder_decoder_paraphraser import (
    LitEncoderDecoderParaphraser,
    LitBert2BertParaphraser,
)
from decoder_only_paraphraser.decoder_only_paraphraser import (
    LitDecoderOnlyParaphraser,
    LitGPT2Paraphraser,
)
import pytorch_lightning as pl
from pprint import pprint
from data_processing.tokenizers import HuggingFaceBertTokenizerWrapper
from argparse import ArgumentParser
import yaml, torch
from pytorch_lightning.callbacks import (
    ModelCheckpoint, 
    EarlyStopping,
    LearningRateMonitor,
)
from transformers import (
    BertTokenizer,
    EncoderDecoderModel,
    EncoderDecoderConfig,
)
from transformers import GPT2LMHeadModel
import sentencepiece as spm

def parse_args():
    parser = ArgumentParser()
    parser = LitEncoderDecoderParaphraser.add_argparse_args(parser)
    parser = ParaphraseDataModule.add_data_module_related_args(parser)
    parser = pl.Trainer.add_argparse_args(parser) 
    parser.add_argument('--checkpoint_file_name', type=str) 
    parser.add_argument('--save_top_k', type=int) 
    parser.add_argument('--diverse_paraphraser_checkpoint_path', type=str) 
    parser.add_argument('--early_stopping_patience', type=int) 
    parser.add_argument('--model_type', type=str) 
    parser.add_argument('--pretrained_path', type=str) 
    parser.add_argument('--tokenizer_type', type=str) 
    parser.add_argument('--tokenizer_path', type=str) 

    args = parser.parse_args()
    return args

def main(args,):
    checkpoint_callback = ModelCheckpoint(
        monitor='val_loss',
        # dirpath=model_checkpoint_path,
        filename=args.checkpoint_file_name + '-{epoch:02d}-{val_loss:.3f}',
        save_top_k=args.save_top_k,
        mode='min',
    )
    early_stopping_callback = EarlyStopping(
        monitor='val_loss',
        min_delta=0.0,
        patience=args.early_stopping_patience,
        verbose=True,
        mode='min',
    )
    learning_rate_monitor_callback = LearningRateMonitor(
        logging_interval=None,
        log_momentum=False,
    )

    trainer = pl.Trainer.from_argparse_args(
        args,
        callbacks=[
            checkpoint_callback, 
            early_stopping_callback,
            learning_rate_monitor_callback,
        ],
        profiler='simple',
    )

    if args.tokenizer_type == 'bert_tokenizer':
        bert_tokenizer = BertTokenizer.from_pretrained(args.tokenizer_path)
        tokenizer = HuggingFaceBertTokenizerWrapper(bert_tokenizer)
    elif args.tokenizer_type == 'sentencepiece':
        tokenizer = spm.SentencePieceProcessor(args.tokenizer_path)

    if args.model_type == 'bert2bert':
        paraphraser = LitBert2BertParaphraser(args)
        # paraphraser.encoder_decoder = EncoderDecoderModel.from_pretrained(args.pretrained_path)
        if args.diverse_paraphraser_checkpoint_path is not None:
            diverse_checkpoint = torch.load(args.diverse_paraphraser_checkpoint_path, map_location='cpu')
            paraphraser.load_state_dict(diverse_checkpoint['state_dict'])
    elif args.model_type == 'gpt2':
        paraphraser = LitGPT2Paraphraser(args)
        # paraphraser.decoder_only = GPT2LMHeadModel.from_pretrained(args.pretrained_path)
        if args.diverse_paraphraser_checkpoint_path is not None:
            diverse_checkpoint = torch.load(args.diverse_paraphraser_checkpoint_path, map_location='cpu')
            paraphraser.load_state_dict(diverse_checkpoint['state_dict'])

    batch_processor = paraphraser.load_batch_processor(tokenizer)

    data_module = ParaphraseDataModule(
        args,
        batch_processor=batch_processor,
    )

    trainer.tune(model=paraphraser, datamodule=data_module)
    trainer.fit(model=paraphraser, datamodule=data_module)

if __name__ == '__main__':
    args = parse_args()

    arg_dict = vars(args)
    pprint(arg_dict)

    main(args)